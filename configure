#! /usr/bin/mbs
/*
	Bubcoin

	Copyright (c) 2020, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import stdopt;
import cc;
import pthread;

set bubcoin_version = (mbs_release_version);

macro bub_import_libs
{
	switch (host)
	{
	case '*-mingw32'
		cc_append_ldflags '-lws2_32 -lcrypt32 -ladvapi32';
		cc_append_cflags '-mno-ms-bitfields';
		break;
	case '*'
		pthread_import_cc;
	};
};

package 'bubcoin' (bubcoin_version)
{
	file (prefix '/bin/bubcoind' mbs_ext_bin)
	{
		cc_define 'BUBCOIN_VERSION' ('"' bubcoin_version '"');
		
		bub_import_libs;
		
		cc_srcdir (mbs_srcdir '/bubcoind');
		cc_link;
	};
	
	file (prefix '/bin/bubcoin-blockdump' mbs_ext_bin)
	{
		cc_define 'BUBCOIN_VERSION' ('"' bubcoin_version '"');
		
		cc_srcdir (mbs_srcdir '/blockdump');
		cc_link;
	};
};