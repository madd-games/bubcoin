/*
	Bubcoin

	Copyright (c) 2020, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "log.h"
#include "thread.h"
#include "config.h"

static Mutex *mtxLog;

void log_init()
{
	mtxLog = mutex_new();
};

static int should_log(int level)
{
	switch (level)
	{
	case LOG_INFO:
		return config.logInfo;
	case LOG_DEBUG:
		return config.logDebug;
	default:
		return 1;
	};
};

void vbublog(int level, const char *format, va_list ap)
{
	if (!should_log(level))
	{
		return;
	};

	mutex_lock(mtxLog);

	char currentTime[256];
	time_t current = time(NULL);

#ifdef _WIN32
	struct tm *tm = localtime(&current);
	strftime(currentTime, 255, "%c", tm);
#else
	struct tm tm;
	localtime_r(&current, &tm);
	strftime(currentTime, 255, "%c", &tm);
#endif // _WIN32

	static const char* logLevelNames[] = {"DEBUG", "INFO", "ERROR"};
	fprintf(stderr, "[bubcoind] [%s] [%s] ", currentTime, logLevelNames[level]);
	vfprintf(stderr, format, ap);
	fprintf(stderr, "\n");

	mutex_unlock(mtxLog);
};

void bublog(int level, const char *format, ...)
{
	va_list ap;
	va_start(ap, format);
	vbublog(level, format, ap);
	va_end(ap);
};

void bub_abort()
{
	bublog(LOG_ERROR, "encountered an unrecoverable error; aborting");
	abort();
};
