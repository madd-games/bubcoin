/*
	Bubcoin

	Copyright (c) 2020, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifdef _WIN32

#include <windows.h>

#include "thread.h"

typedef struct
{
	thread_entry_t entry;
	void* param;
} ThreadEntryData;

struct Mutex_
{
	HANDLE handle;
};

struct Thread_
{
	HANDLE handle;
};

Mutex* mutex_new()
{
	Mutex *mtx = (Mutex*) malloc(sizeof(Mutex));
	mtx->handle = CreateMutex(NULL, FALSE, NULL);
	return mtx;
};

void mutex_destroy(Mutex *mtx)
{
	CloseHandle(mtx->handle);
	free(mtx);
};

void mutex_lock(Mutex *mtx)
{
	WaitForSingleObject(mtx->handle, INFINITE);
};

void mutex_unlock(Mutex *mtx)
{
	ReleaseMutex(mtx->handle);
};

static DWORD WINAPI thread_entry_proc(void *context)
{
	ThreadEntryData *data = (ThreadEntryData*) context;
	thread_entry_t entry = data->entry;
	void *param = data->param;
	free(data);

	entry(param);
	return 0;
}
Thread* thread_start(thread_entry_t entry, void *param)
{
	Thread *th = (Thread*) malloc(sizeof(Thread));
	ThreadEntryData *data = (ThreadEntryData*) malloc(sizeof(ThreadEntryData));

	data->entry = entry;
	data->param = param;

	th->handle = CreateThread(NULL, 0, thread_entry_proc, data, 0, NULL);
	return th;
};

void thread_detach(Thread *th)
{
	CloseHandle(th->handle);
	free(th);
};

void thread_wait(Thread *th)
{
	WaitForSingleObject(th->handle, INFINITE);
	thread_detach(th);
};

void thread_exit()
{
	ExitThread(0);
};

#endif // _WIN32
