/*
	Bubcoin

	Copyright (c) 2020, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "block.h"
#include "log.h"
#include "ether.h"
#include "net.h"
#include "config.h"

#define	BLOCK_DIR_PREFIX		"blocks/"

#define	GENESIS_BLOCK_SIZE		122
static const uint8_t genesisBlock[GENESIS_BLOCK_SIZE] = {
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00, 0xB0, 0xFF, 0xFF, 0xFF, 0x91, 0x17, 0x3D, 0x02, 0x00, 0x00, 0x00, 0x00,
};

typedef struct
{
	sha256_t partial;
	uint64_t startNonce;
	uint64_t stepNonce;
	hash_t maxHash;
	uint64_t *nonceOut;
	hash_t onBlock;
} MinerHashInfo;

static hash_t* heads;
static int numHeads;

static pubkey_t myMinerID;
static privkey_t myPrivKey;
static Mutex* mtxHeads;

static uint64_t blockProgress;

static void purge_easy_heads();
static void block_read_data(hash_t *hash, void **dataOut, size_t *sizeOut);

void write_heads_to_file()
{
	mutex_lock(mtxHeads);
	
	FILE *fp = fopen(BLOCK_DIR_PREFIX "HEADS", "wb");
	if (fp == NULL)
	{
		remove(BLOCK_DIR_PREFIX "HEADS");
		bublog(LOG_ERROR, "failed to open HEADS file for write");
		bub_abort();
	}
	if (fwrite(heads, sizeof(hash_t), numHeads, fp) != numHeads)
	{
		remove(BLOCK_DIR_PREFIX "HEADS");
		bublog(LOG_ERROR, "failed to write to the HEADS file");
		bub_abort();
	}
	fclose(fp);
	
	mutex_unlock(mtxHeads);
};

void block_init()
{
	mtxHeads = mutex_new();
	
	FILE *fp = fopen(BLOCK_DIR_PREFIX "HEADS", "rb");
	if (fp != NULL)
	{
		fseek(fp, 0, SEEK_END);
		numHeads = ftell(fp) / sizeof(hash_t);
		fseek(fp, 0, SEEK_SET);
		
		heads = (hash_t*) malloc(sizeof(hash_t) * numHeads);
		if (fread(heads, sizeof(hash_t), numHeads, fp) != numHeads)
		{
			bublog(LOG_ERROR, "fread() failed on the HEADS file");
			bub_abort();
		};
		fclose(fp);
		
		bublog(LOG_INFO, "block tracker: read in %d heads", numHeads);
		purge_easy_heads();
		write_heads_to_file();
	}
	else
	{
		// no HEADS file; create the genesis block and initialize
		bublog(LOG_INFO, "block tracker: no HEADS file: initializing blank blockchain");
		
		sha256_t hasher;
		sha256_init(&hasher);
		sha256_feed(&hasher, genesisBlock, GENESIS_BLOCK_SIZE);
		
		hash_t genesisHash;
		sha256_to_hash(&hasher, &genesisHash);
		
		char hashAsHex[sizeof(hash_t) * 2 + 1];
		hash_to_string(&genesisHash, hashAsHex);
		
		char blockName[256];
		char notesName[256];
		
		sprintf(blockName, BLOCK_DIR_PREFIX "%s.blk", hashAsHex);
		sprintf(notesName, BLOCK_DIR_PREFIX "%s.not", hashAsHex);
		
		fp = fopen(blockName, "wb");
		if (fp == NULL)
		{
			bublog(LOG_ERROR, "failed to open %s for write", blockName);
			bub_abort();
		};
		
		if (fwrite(genesisBlock, 1, GENESIS_BLOCK_SIZE, fp) != GENESIS_BLOCK_SIZE)
		{
			bublog(LOG_ERROR, "failed to write to %s", blockName);
			bub_abort();
		};
		fclose(fp);
		
		fp = fopen(notesName, "wb");
		if (fp == NULL)
		{
			bublog(LOG_ERROR, "failed to create %s", notesName);
			bub_abort();
		};
		
		BlockNotes notes;
		memset(&notes, 0, sizeof(BlockNotes));
		BlockFooterEntry *footer = (BlockFooterEntry*) (genesisBlock + sizeof(BlockHeaderEntry));
		notes.difficultyLow = footer->difficulty;
		notes.localDifficulty = footer->difficulty;
		
		if (fwrite(&notes, sizeof(BlockNotes), 1, fp) != 1)
		{
			bublog(LOG_ERROR, "failed to write to %s", notesName);
			bub_abort();
		};
		fclose(fp);
		
		heads = (hash_t*) malloc(sizeof(hash_t));
		memcpy(heads, &genesisHash, sizeof(hash_t));
		numHeads = 1;
		
		write_heads_to_file();
	};
};

int block_read_notes(hash_t *hash, BlockNotes *notes)
{
	char hashAsHex[sizeof(hash_t) * 2 + 1];
	hash_to_string(hash, hashAsHex);
	
	char notesName[256];
	
	sprintf(notesName, BLOCK_DIR_PREFIX "%s.not", hashAsHex);
	
	FILE *fp = fopen(notesName, "rb");
	if (fp == NULL)
	{
		bublog(LOG_ERROR, "failed to open %s for reading", notesName);
		return -1;
	};
	
	if (fread(notes, sizeof(BlockNotes), 1, fp) != 1)
	{
		bublog(LOG_ERROR, "failed to read from %s", notesName);
		return -1;
	};
	
	fclose(fp);
	return 0;
};

int block_is_difficulty_correct(const void *block, size_t size)
{
	BlockFooterEntry* footer = (BlockFooterEntry*) ((char*) block + size - sizeof(BlockFooterEntry));
	if (footer->type != BLK_ENT_FOOTER)
	{
		return 0;
	};
	
	hash_t maxHash;
	
	memset(&maxHash, 0xFF, sizeof(hash_t));
	int i;
	for (i=0; i<8; i++)
	{
		maxHash.bytes[7-i] = ~(footer->difficulty >> (8*i));
	};
	
	hash_t actualHash;
	sha256_t hasher;
	sha256_init(&hasher);
	sha256_feed(&hasher, block, size);
	sha256_to_hash(&hasher, &actualHash);
	
	return memcmp(&actualHash, &maxHash, sizeof(hash_t)) <= 0;
};

static void miner_hash_thread(void *data)
{
	MinerHashInfo *info = (MinerHashInfo*) data;
	
	uint64_t i;
	for (i=0;;i++)
	{
		uint64_t nonce = info->startNonce + i * info->stepNonce;
		blockProgress = nonce;
		
		sha256_t full = info->partial;
		sha256_feed(&full, &nonce, 8);
		
		hash_t result;
		sha256_to_hash(&full, &result);
		
		if (memcmp(&result, &info->maxHash, sizeof(hash_t)) <= 0)
		{
			*info->nonceOut = nonce;
			break;
		};

		if (i != 0 && (i & 0xFF) == 0)
		{
			hash_t strongest;
			block_get_strongest_head(&strongest);
			if (memcmp(&info->onBlock, &strongest, sizeof(hash_t)) != 0)
			{
				// give up the block
				break;
			};
			
			if (*info->nonceOut != 0) break;
		};
	};
};

int block_mine(void *block, size_t size)
{
	BlockHeaderEntry* header = (BlockHeaderEntry*) block;
	
	BlockFooterEntry* footer = (BlockFooterEntry*) ((char*) block + size - sizeof(BlockFooterEntry));
	hash_t maxHash;
	
	memset(&maxHash, 0xFF, sizeof(hash_t));
	int i;
	for (i=0; i<8; i++)
	{
		maxHash.bytes[7-i] = ~(footer->difficulty >> (8*i));
	};
	
	sha256_t partial;
	sha256_init(&partial);
	sha256_feed(&partial, block, size-8);
	
	footer->nonce = 0;
	
	int numMinerThreads = config.minerThreads;
	MinerHashInfo info[numMinerThreads];
	Thread* threads[numMinerThreads];
	
	for (i=0; i<numMinerThreads; i++)
	{
		info[i].partial = partial;
		info[i].startNonce = i;
		info[i].stepNonce = numMinerThreads;
		info[i].maxHash = maxHash;
		info[i].nonceOut = &footer->nonce;
		info[i].onBlock = header->prevBlockHash;
		threads[i] = thread_start(miner_hash_thread, &info[i]);
	};
	
	for (i=0; i<numMinerThreads; i++)
	{
		thread_wait(threads[i]);
	};
	
	if (footer->nonce != 0)
	{
		return 0;
	};
	
	return -1;
};

void block_init_miner()
{
	FILE *fp = fopen("miner/public.key", "rb");
	if (fp == NULL)
	{
		bublog(LOG_INFO, "miner wallet not found; creating");
		
		if (sig_genkey(&myPrivKey, &myMinerID) != 0)
		{
			bublog(LOG_ERROR, "failed to create miner wallet");
			bub_abort();
		};
		
		fp = fopen("miner/private.key", "wb");
		if (fp == NULL)
		{
			bublog(LOG_ERROR, "failed to write to miner/private.key");
			bub_abort();
		};
		
		if (fwrite(&myPrivKey, 1, sizeof(privkey_t), fp) != sizeof(privkey_t))
		{
			bublog(LOG_ERROR, "failed to write private key");
			remove("miner/private.key");
			bub_abort();
		};
		
		fclose(fp);
		
		fp = fopen("miner/public.key", "wb");
		if (fp == NULL)
		{
			bublog(LOG_ERROR, "failed to write to miner/public.key");
			bub_abort();
		};
		
		if (fwrite(&myMinerID, 1, sizeof(pubkey_t), fp) != sizeof(pubkey_t))
		{
			bublog(LOG_ERROR, "failed to write public key");
			remove("miner/public.key");
			bub_abort();
		};
		
		fclose(fp);
	}
	else
	{
		if (fread(&myMinerID, 1, sizeof(pubkey_t), fp) != sizeof(pubkey_t))
		{
			bublog(LOG_ERROR, "failed to read the public key");
			bub_abort();
		};
		
		fclose(fp);
		
		fp = fopen("miner/private.key", "rb");
		if (fp == NULL)
		{
			bublog(LOG_ERROR, "failed to read private key");
			bub_abort();
		};
		
		if (fread(&myPrivKey, 1, sizeof(privkey_t), fp) != sizeof(privkey_t))
		{
			bublog(LOG_ERROR, "failed to read private key");
			bub_abort();
		};
		
		fclose(fp);
		bublog(LOG_INFO, "read in miner wallet data");
	};
};

static void purge_easy_heads()
{
	hash_t strongest;
	block_get_strongest_head(&strongest);
	
	BlockNotes strongNotes;
	if (block_read_notes(&strongest, &strongNotes) != 0)
	{
		bublog(LOG_ERROR, "purge_easy_heads: failed to read strongest block's notes");
		return;
	};
	
	uint64_t deltaHigh = ~(strongNotes.localDifficulty >> 61);
	uint64_t deltaLow = ~(strongNotes.localDifficulty << 3);
	
	uint64_t minDiffHigh = strongNotes.difficultyHigh + deltaHigh + !!(strongNotes.difficultyLow > ~deltaLow);
	uint64_t minDiffLow = strongNotes.difficultyLow + deltaLow;
	
	if (minDiffLow == 0xFFFFFFFFFFFFFFFF)
	{
		minDiffLow = 0;
		minDiffHigh++;
	}
	else
	{
		minDiffLow++;
	};
	
	if (minDiffHigh & (1ULL << 63))
	{
		// negative expectation, so don't purge any heads
		return;
	};
	
	mutex_lock(mtxHeads);
	
	hash_t *newHeadList = (hash_t*) malloc(sizeof(hash_t) * numHeads);
	int numNewHeads = 0;
	
	int i;
	for (i=0; i<numHeads; i++)
	{
		char hashAsStr[HASH_STRING_SIZE];
		hash_to_string(&heads[i], hashAsStr);
		
		BlockNotes notes;
		if (block_read_notes(&heads[i], &notes) != 0)
		{
			mutex_unlock(mtxHeads);
			free(newHeadList);
			bublog(LOG_ERROR, "purge_easy_heads: failed to read notes for %s", hashAsStr);
			return;
		};
		
		if (notes.difficultyHigh > minDiffHigh || (notes.difficultyHigh == minDiffHigh && notes.difficultyLow >= minDiffLow))
		{
			int newIndex = numNewHeads++;
			memcpy(&newHeadList[newIndex], &heads[i], sizeof(hash_t));
		}
		else
		{
			bublog(LOG_INFO, "purge_easy_heads: purging %s (total difficulty %016" PRIx64 "%016" PRIx64
				", needs to be at least %016" PRIx64 "%016" PRIx64 ")", hashAsStr,
				notes.difficultyHigh, notes.difficultyLow, minDiffHigh, minDiffLow);
		};
	};
	
	newHeadList = realloc(newHeadList, sizeof(hash_t) * numNewHeads);
	free(heads);
	heads = newHeadList;
	numHeads = numNewHeads;
	mutex_unlock(mtxHeads);
};

int block_add(void *block, size_t size)
{
	if (size < sizeof(BlockHeaderEntry) + sizeof(BlockFooterEntry))
	{
		bublog(LOG_DEBUG, "block_add: block too small to fit header and footer");
		return -1;
	};
	
	BlockHeaderEntry *header = (BlockHeaderEntry*) block;
	if (header->type != BLK_ENT_HEADER)
	{
		bublog(LOG_DEBUG, "block_add: invalid header entry type");
		return -1;
	};
	
	// verify that the prevBlockHash is one of the heads
	mutex_lock(mtxHeads);
	int i;
	for (i=0; i<numHeads; i++)
	{
		if (memcmp(&heads[i], &header->prevBlockHash, sizeof(hash_t)) == 0)
		{
			break;
		};
	};
	
	if (i == numHeads)
	{
		bublog(LOG_DEBUG, "block_add: block does not belong to any head");
		mutex_unlock(mtxHeads);
		return -1;
	};
	
	if (!block_is_difficulty_correct(block, size))
	{
		bublog(LOG_ERROR, "block_add: block difficulty is invalid");
		mutex_unlock(mtxHeads);
		return -1;
	};
	
	// compute the hash
	sha256_t ctx;
	sha256_init(&ctx);
	sha256_feed(&ctx, block, size);
	hash_t newBlockHash;
	sha256_to_hash(&ctx, &newBlockHash);
	
	// check if we already have it
	if (block_exists(&newBlockHash))
	{
		mutex_unlock(mtxHeads);
		return -1;
	};
	
	// read in the previous block's notes
	BlockNotes prevNotes;
	if (block_read_notes(&header->prevBlockHash, &prevNotes) != 0)
	{
		bublog(LOG_ERROR, "block_add: failed to read previous block's notes");
		mutex_unlock(mtxHeads);
		return -1;
	};
	
	// attach the head
	int newHeadIndex = numHeads++;
	heads = (hash_t*) realloc(heads, sizeof(hash_t) * numHeads);
	memcpy(&heads[newHeadIndex], &newBlockHash, sizeof(hash_t));
	
	// write to the file
	char hashAsHex[sizeof(hash_t) * 2 + 1];
	hash_to_string(&newBlockHash, hashAsHex);
	
	char blockName[256];
	char notesName[256];
	
	sprintf(blockName, BLOCK_DIR_PREFIX "%s.blk", hashAsHex);
	sprintf(notesName, BLOCK_DIR_PREFIX "%s.not", hashAsHex);
	
	FILE *fp = fopen(blockName, "wb");
	if (fp == NULL)
	{
		bublog(LOG_ERROR, "failed to open %s for write", blockName);
		bub_abort();
		return -1;
	};
	
	if (fwrite(block, 1, size, fp) != size)
	{
		bublog(LOG_ERROR, "failed to write to %s", blockName);
		bub_abort();
		return -1;
	};
	fclose(fp);
	
	fp = fopen(notesName, "wb");
	if (fp == NULL)
	{
		bublog(LOG_ERROR, "failed to create %s", notesName);
		bub_abort();
		return -1;
	};
	
	BlockFooterEntry *footer = (BlockFooterEntry*) ((char*) block + size - sizeof(BlockFooterEntry));
	
	BlockNotes newNotes;
	memset(&newNotes, 0, sizeof(BlockNotes));
	newNotes.difficultyHigh = prevNotes.difficultyHigh;
	newNotes.difficultyLow = prevNotes.difficultyLow + footer->difficulty;
	newNotes.localDifficulty = footer->difficulty;
	newNotes.blockNum = prevNotes.blockNum + 1;
	
	if ((~footer->difficulty) < prevNotes.difficultyLow)
	{
		newNotes.difficultyHigh++;
	};
	
	if (fwrite(&newNotes, sizeof(BlockNotes), 1, fp) != 1)
	{
		bublog(LOG_ERROR, "failed to write to %s", notesName);
		bub_abort();
		return -1;
	};
	fclose(fp);
	
	mutex_unlock(mtxHeads);
	purge_easy_heads();
	write_heads_to_file();
	
	bublog(LOG_INFO, "accepted block %s", hashAsHex);
	net_announce_block(&newBlockHash, block, size);
	return 0;
};

void block_get_strongest_head(hash_t *hash)
{
	mutex_lock(mtxHeads);
	
	int mostDifficult = -1;
	uint64_t highestHigh;
	uint64_t highestLow;
	
	int i;
	for (i=0; i<numHeads; i++)
	{
		BlockNotes notes;
		if (block_read_notes(&heads[i], &notes) != 0)
		{
			bub_abort();
		};
		
		if (mostDifficult == -1 || notes.difficultyHigh > highestHigh
			|| (notes.difficultyHigh == highestHigh && notes.difficultyLow > highestLow))
		{
			mostDifficult = i;
			highestHigh = notes.difficultyHigh;
			highestLow = notes.difficultyLow;
		};
	};
	
	if (mostDifficult == -1)
	{
		bublog(LOG_ERROR, "fatal: no heads");
		bub_abort();
	};
	
	memcpy(hash, &heads[mostDifficult], sizeof(hash_t));
	
	mutex_unlock(mtxHeads);
};

int block_mine_next()
{
	hash_t prevHash;
	block_get_strongest_head(&prevHash);
	
	BlockNotes prevNotes;
	if (block_read_notes(&prevHash, &prevNotes) != 0)
	{
		bub_abort();
	};
	
	uint64_t difficulty = prevNotes.localDifficulty;
	if (prevNotes.blockNum != 0 && (prevNotes.blockNum % 64) == 0)
	{
		uint64_t latestTime = 0;
		uint64_t earliestTime = 0;
		uint64_t averageDifficulty = 0;
		
		hash_t hash = prevHash;
		int i;
		for (i=0; i<64; i++)
		{
			void *block;
			size_t blockSize;
			block_read_data(&hash, &block, &blockSize);
			BlockHeaderEntry *header = (BlockHeaderEntry*) block;
			BlockFooterEntry *footer = (BlockFooterEntry*) ((char*) block + blockSize - sizeof(BlockFooterEntry));
			
			if (i == 0)
			{
				latestTime = header->timestamp;
			}
			else if (i == 63)
			{
				earliestTime = header->timestamp;
			};
			
			free(block);
			
			averageDifficulty += (footer->difficulty >> 6);
			block_get_prev(&hash);
		};
		
		uint64_t averageTime = (latestTime - earliestTime) >> 6;
		if (averageTime == 0) averageTime = 1;
		if (averageDifficulty == 0) averageDifficulty = 1;
		if (averageTime < DESIRED_MINING_INTERVAL)
		{
			while (averageTime < DESIRED_MINING_INTERVAL)
			{
				averageDifficulty >>= 1;
				averageDifficulty |= 0x8000000000000000;
				averageTime <<= 1;
			};
		}
		else if (averageTime > DESIRED_MINING_INTERVAL)
		{
			while (averageTime > 2*DESIRED_MINING_INTERVAL)
			{
				averageDifficulty <<= 1;
				averageTime >>= 1;
			};
		};
		
		difficulty = averageDifficulty;
	};
	
	size_t currentBlockSize = sizeof(BlockHeaderEntry) + sizeof(BlockFooterEntry);
	void *block = malloc(currentBlockSize);
	
	BlockHeaderEntry *header = (BlockHeaderEntry*) block;
	header->type = BLK_ENT_HEADER;
	memcpy(&header->prevBlockHash, &prevHash, sizeof(hash_t));
	memcpy(&header->minerID, &myMinerID, sizeof(pubkey_t));
	header->timestamp = (uint64_t) time(NULL);
	
	ether_select_for_block(&block, &currentBlockSize);
	
	BlockFooterEntry *footer = (BlockFooterEntry*) ((char*) block + currentBlockSize - sizeof(BlockFooterEntry));
	footer->type = BLK_ENT_FOOTER;
	footer->difficulty = difficulty;
	
	// mining time
	char prevHex[sizeof(hash_t) * 2 + 1];
	hash_to_string(&prevHash, prevHex);
	bublog(LOG_INFO, "created and mining a block off %s (difficulty %016" PRIx64 ")", prevHex, difficulty);
	
	time_t startTime = time(NULL);
	if (block_mine(block, currentBlockSize) == -1)
	{
		free(block);
		return -1;
	};
	time_t endTime = time(NULL);
	
	sha256_t ctx;
	sha256_init(&ctx);
	sha256_feed(&ctx, block, currentBlockSize);
	hash_t newHash;
	sha256_to_hash(&ctx, &newHash);
	
	char hashAsHex[sizeof(hash_t) * 2 + 1];
	hash_to_string(&newHash, hashAsHex);
	bublog(LOG_INFO, "mined block %s, took %d seconds", hashAsHex, (int) (endTime-startTime));
	
	int result = block_add(block, currentBlockSize);
	free(block);
	return result;
};

static void block_get_file(hash_t *hash, const char *which, char *result)
{
	char hashAsHex[sizeof(hash_t) * 2 + 1];
	hash_to_string(hash, hashAsHex);
	
	sprintf(result, BLOCK_DIR_PREFIX "%s.%s", hashAsHex, which);
};

int block_get_prev(hash_t *hash)
{
	char blockFileName[256];
	block_get_file(hash, "blk", blockFileName);
	
	FILE *fp = fopen(blockFileName, "rb");
	if (fp == NULL)
	{
		bublog(LOG_ERROR, "block_get_prev: failed to open %s for reading", blockFileName);
		return -1;
	};
	
	BlockHeaderEntry header;
	if (fread(&header, sizeof(header), 1, fp) != 1)
	{
		bublog(LOG_ERROR, "block_get_prev: failed to read header from %s", blockFileName);
		return -1;
	};
	
	fclose(fp);
	
	if (header.type != BLK_ENT_HEADER)
	{
		bublog(LOG_ERROR, "block_get_prev: %s: invalid type for header entry", blockFileName);
		return -1;
	};
	
	memcpy(hash, &header.prevBlockHash, sizeof(hash_t));
	return 0;
};

int block_is_null(hash_t *hash)
{
	int i;
	for (i=0; i<sizeof(hash_t); i++)
	{
		if (hash->bytes[i] != 0) return 0;
	};
	
	return 1;
};

static void block_read_data(hash_t *hash, void **dataOut, size_t *sizeOut)
{
	char blockFileName[256];
	block_get_file(hash, "blk", blockFileName);
	
	FILE *fp = fopen(blockFileName, "rb");
	if (fp == NULL)
	{
		bublog(LOG_ERROR, "failed to open %s for reading", blockFileName);
		bub_abort();
	};
	
	fseek(fp, 0, SEEK_END);
	size_t size = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	
	void *data = malloc(size);
	if (fread(data, 1, size, fp) != size)
	{
		bublog(LOG_ERROR, "failed to read %s", blockFileName);
		bub_abort();
	};
	
	fclose(fp);
	
	*dataOut = data;
	*sizeOut = size;
};

int block_read(hash_t *hash, void **dataOut, size_t *sizeOut)
{
	char blockFileName[256];
	block_get_file(hash, "blk", blockFileName);
	
	FILE *fp = fopen(blockFileName, "rb");
	if (fp == NULL)
	{
		bublog(LOG_ERROR, "failed to open %s for reading", blockFileName);
		return -1;
	};
	
	fseek(fp, 0, SEEK_END);
	size_t size = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	
	void *data = malloc(size);
	if (fread(data, 1, size, fp) != size)
	{
		bublog(LOG_ERROR, "failed to read %s", blockFileName);
		free(data);
		return -1;
	};
	
	fclose(fp);
	
	*dataOut = data;
	*sizeOut = size;
	
	return 0;
};

int block_is_spent(hash_t *scanFrom, pubkey_t *spender, hash_t *inputBlockHash, uint64_t entry)
{
	hash_t scan;
	for (scan=*scanFrom; !block_is_null(&scan); block_get_prev(&scan))
	{
		if (memcmp(inputBlockHash, &scan, sizeof(hash_t)) == 0)
		{
			// couldn't have been used in its own block or before,
			// so we know by now that it's unspent
			return 0;
		};
		
		void *block;
		size_t blockSize;
		block_read_data(&scan, &block, &blockSize);
		
		void *bscan = (char*) block + sizeof(BlockHeaderEntry);
		while (*((uint8_t*)bscan) == BLK_ENT_TRANSACTION)
		{
			BlockTransactionEntryHeader *header = (BlockTransactionEntryHeader*) bscan;
			void *nextPtr = (char*) bscan + sizeof(BlockTransactionEntryHeader)
						+ header->numInputs * sizeof(BlockTransactionEntryInput)
						+ header->numOutputs * sizeof(BlockTransactionEntryOutput);
			
			BlockTransactionEntryInput *inputs = (BlockTransactionEntryInput*) ((char*) bscan + sizeof(BlockTransactionEntryHeader));
			if (memcmp(spender, &header->spenderID, sizeof(pubkey_t)) == 0)
			{
				uint64_t i;
				for (i=0; i<header->numInputs; i++)
				{
					if (inputs[i].entryIndex == entry && memcmp(&inputs[i].inputBlockHash, inputBlockHash, sizeof(hash_t)) == 0)
					{
						// spent
						free(block);
						return 1;
					};
				};
			};
			
			bscan = nextPtr;
		};
		
		free(block);
	};
	
	return 0;
};

uint64_t block_get_value(hash_t *scanFrom, pubkey_t *spender, hash_t *inputBlockHash, uint64_t entry)
{
	hash_t scan;
	for (scan=*scanFrom; !block_is_null(&scan); block_get_prev(&scan))
	{
		void *block;
		size_t blockSize;
		block_read_data(&scan, &block, &blockSize);
		
		if (memcmp(inputBlockHash, &scan, sizeof(hash_t)) == 0)
		{
			// not spent, and found the block
			if (entry == 0)
			{
				// claims to have mined it
				BlockHeaderEntry *header = (BlockHeaderEntry*) block;
				if (memcmp(&header->minerID, spender, sizeof(pubkey_t)) == 0)
				{
					// correct, and the value is a full bubcoin
					free(block);
					return FULL_BUBCOIN;
				}
				else
				{
					// lied
					free(block);
					return 0;
				};
			};
			
			void *bscan = (char*) block + sizeof(BlockHeaderEntry);
			uint64_t currentEntry = 1;
			
			while (*((uint8_t*)bscan) == BLK_ENT_TRANSACTION)
			{
				BlockTransactionEntryHeader *header = (BlockTransactionEntryHeader*) bscan;
				void *nextPtr = (char*) bscan + sizeof(BlockTransactionEntryHeader)
							+ header->numInputs * sizeof(BlockTransactionEntryInput)
							+ header->numOutputs * sizeof(BlockTransactionEntryOutput);
				
				if (currentEntry == entry)
				{
					// claims it's within this entry
					BlockTransactionEntryOutput *outputs = (BlockTransactionEntryOutput*)
						((char*) bscan + sizeof(BlockTransactionEntryHeader)
						+ header->numInputs * sizeof(BlockTransactionEntryInput));
					uint64_t i;
					for (i=0; i<header->numOutputs; i++)
					{
						if (memcmp(&outputs[i].targetID, spender, sizeof(pubkey_t)) == 0)
						{
							// true
							uint64_t result = outputs[i].value;
							free(block);
							return result;
						};
					};
					
					// lied
					free(block);
					return 0;
				};
				
				bscan = nextPtr;
				currentEntry++;
			};
			
			// lied
			free(block);
			return 0;
		};
		
		void *bscan = (char*) block + sizeof(BlockHeaderEntry);
		while (*((uint8_t*)bscan) == BLK_ENT_TRANSACTION)
		{
			BlockTransactionEntryHeader *header = (BlockTransactionEntryHeader*) bscan;
			void *nextPtr = (char*) bscan + sizeof(BlockTransactionEntryHeader)
						+ header->numInputs * sizeof(BlockTransactionEntryInput)
						+ header->numOutputs * sizeof(BlockTransactionEntryOutput);
			
			BlockTransactionEntryInput *inputs = (BlockTransactionEntryInput*) ((char*) bscan + sizeof(BlockTransactionEntryHeader));
			if (memcmp(spender, &header->spenderID, sizeof(pubkey_t)) == 0)
			{
				uint64_t i;
				for (i=0; i<header->numInputs; i++)
				{
					if (inputs[i].entryIndex == entry && memcmp(&inputs[i].inputBlockHash, inputBlockHash, sizeof(hash_t)) == 0)
					{
						// spent
						free(block);
						return 0;
					};
				};
			};
			
			bscan = nextPtr;
		};
		
		free(block);
	};
	
	return 0;
};

uint64_t block_get_my_balance()
{
	uint64_t balance = 0;
	
	hash_t head;
	block_get_strongest_head(&head);
	
	hash_t scan = head;
	while (!block_is_null(&scan))
	{
		void *block;
		size_t blockSize;
		block_read_data(&scan, &block, &blockSize);
		
		BlockHeaderEntry *header = (BlockHeaderEntry*) block;
		if (memcmp(&header->minerID, &myMinerID, sizeof(pubkey_t)) == 0)
		{
			balance += FULL_BUBCOIN;
		};
		
		uint64_t entryNum = 1;
		void *bscan = ((char*) block + sizeof(BlockHeaderEntry));
		while (*((char*)bscan) == BLK_ENT_TRANSACTION)
		{
			BlockTransactionEntryHeader *th = (BlockTransactionEntryHeader*) bscan;
			void *following = (char*) bscan + sizeof(BlockTransactionEntryHeader) + sizeof(BlockTransactionEntryInput) * th->numInputs
						+ sizeof(BlockTransactionEntryOutput) * th->numOutputs;
			
			BlockTransactionEntryOutput *outputs = (BlockTransactionEntryOutput*)
				((char*) bscan + sizeof(BlockTransactionEntryHeader) + th->numInputs * sizeof(BlockTransactionEntryInput));
			
			int isMine = (memcmp(&th->spenderID, &myMinerID, sizeof(pubkey_t)) == 0);
			
			uint64_t i;
			for (i=0; i<th->numOutputs; i++)
			{
				if (memcmp(&outputs[i].targetID, &myMinerID, sizeof(pubkey_t)) == 0)
				{
					if (!isMine)
					{
						balance += outputs[i].value;
					};
				}
				else
				{
					if (isMine)
					{
						balance -= outputs[i].value;
					};
				};
			};
			
			bscan = following;
			entryNum++;
		};
		
		scan = header->prevBlockHash;
		free(block);
	};
	
	return balance;
};

int block_make_spend(uint64_t amount, pubkey_t *target, void **transOut, size_t *sizeOut)
{
	// TODO: check the ether to verify that we aren't attempting to double-spend
	if (memcmp(target, &myMinerID, sizeof(pubkey_t)) == 0)
	{
		// cannot send money to yourself
		return -1;
	};
	
	BlockTransactionEntryInput *inputs = NULL;
	size_t numInputs = 0;
	uint64_t currentSpend = 0;
	
	hash_t head;
	block_get_strongest_head(&head);
	
	hash_t scan = head;
	while (!block_is_null(&scan) && currentSpend < amount)
	{
		void *block;
		size_t blockSize;
		block_read_data(&scan, &block, &blockSize);
		
		BlockHeaderEntry *header = (BlockHeaderEntry*) block;
		if (memcmp(&header->minerID, &myMinerID, sizeof(pubkey_t)) == 0)
		{
			// we mined this block
			if (!block_is_spent(&head, &header->minerID, &scan, 0))
			{
				currentSpend += FULL_BUBCOIN;
				
				size_t inputIndex = numInputs++;
				inputs = (BlockTransactionEntryInput*) realloc(inputs, sizeof(BlockTransactionEntryInput) * numInputs);
				inputs[inputIndex].inputBlockHash = scan;
				inputs[inputIndex].entryIndex = 0;
			};
		};
		
		uint64_t entryNum = 1;
		void *bscan = ((char*) block + sizeof(BlockHeaderEntry));
		while (*((char*)bscan) == BLK_ENT_TRANSACTION)
		{
			BlockTransactionEntryHeader *th = (BlockTransactionEntryHeader*) bscan;
			void *following = (char*) bscan + sizeof(BlockTransactionEntryHeader) + sizeof(BlockTransactionEntryInput) * th->numInputs
						+ sizeof(BlockTransactionEntryOutput) * th->numOutputs;
			
			BlockTransactionEntryOutput *outputs = (BlockTransactionEntryOutput*)
				((char*) bscan + sizeof(BlockTransactionEntryHeader) + th->numInputs * sizeof(BlockTransactionEntryInput));
			
			uint64_t i;
			for (i=0; i<th->numOutputs; i++)
			{
				if (memcmp(&outputs[i].targetID, &myMinerID, sizeof(pubkey_t)) == 0)
				{
					if (!block_is_spent(&head, &myMinerID, &scan, entryNum))
					{
						currentSpend += outputs[i].value;
						
						size_t inputIndex = numInputs++;
						inputs = (BlockTransactionEntryInput*) realloc(inputs, sizeof(BlockTransactionEntryInput) * numInputs);
						inputs[inputIndex].inputBlockHash = scan;
						inputs[inputIndex].entryIndex = entryNum;
					};
				};
			};
			
			bscan = following;
			entryNum++;
		};
		
		scan = header->prevBlockHash;
		free(block);
	};
	
	if (currentSpend < amount)
	{
		free(inputs);
		return -1;
	};
	
	size_t transSize = sizeof(BlockTransactionEntryHeader)
		+ numInputs * sizeof(BlockTransactionEntryInput)
		+ 2 * sizeof(BlockTransactionEntryOutput);
	if (currentSpend == amount)
	{
		// don't need change
		transSize -= sizeof(BlockTransactionEntryOutput);
	};
	
	void *trans = malloc(transSize);
	
	BlockTransactionEntryHeader *header = (BlockTransactionEntryHeader*) trans;
	memcpy((char*) trans + sizeof(BlockTransactionEntryHeader), inputs, numInputs * sizeof(BlockTransactionEntryInput));
	free(inputs);
	BlockTransactionEntryOutput *outputs = (BlockTransactionEntryOutput*) ((char*) trans
		+ sizeof(BlockTransactionEntryHeader)
		+ numInputs * sizeof(BlockTransactionEntryInput));
	
	outputs[0].targetID = *target;
	outputs[0].value = amount;
	
	if (currentSpend != amount)
	{
		outputs[1].targetID = myMinerID;
		outputs[1].value = currentSpend - amount;
	};
	
	header->type = BLK_ENT_TRANSACTION;
	header->spenderID = myMinerID;
	memset(&header->signature, 0, sizeof(sig_t));		// zero out signature prior to signing, as per protocol
	header->numInputs = numInputs;
	header->numOutputs = (currentSpend == amount) ? 1 : 2;
	header->timestamp = (uint64_t) time(NULL);
	
	// sign
	sha256_t ctx;
	sha256_init(&ctx);
	sha256_feed(&ctx, trans, transSize);
	sig_sign(&myPrivKey, &ctx, &header->signature);
	
	// done!
	*transOut = trans;
	*sizeOut = transSize;
	return 0;
};

hash_t* block_get_all_heads(int *countOut)
{
	mutex_lock(mtxHeads);
	
	hash_t *result = (hash_t*) malloc(sizeof(hash_t) * numHeads);
	memcpy(result, heads, sizeof(hash_t) * numHeads);
	*countOut = numHeads;
	
	mutex_unlock(mtxHeads);
	return result;
};

int block_exists(hash_t *hash)
{
	char blockFileName[256];
	block_get_file(hash, "blk", blockFileName);
	
	FILE *fp = fopen(blockFileName, "rb");
	if (fp == NULL)
	{
		return 0;
	};
	
	fclose(fp);
	return 1;
};

static void block_miner_thread(void *ignore)
{
	(void) ignore;
	
	if (config.minerThreads == 0)
	{
		bublog(LOG_INFO, "block mining is disabled");
		return;
	};
	
	bublog(LOG_INFO, "block miner started");
	while (1)
	{
		block_mine_next();
	};
};

void block_start_miner_thread()
{
	thread_detach(thread_start(block_miner_thread, NULL));
};

void block_get_my_public_key(pubkey_t *key)
{
	memcpy(key, &myMinerID, sizeof(pubkey_t));
};

uint64_t block_get_progress()
{
	return blockProgress;
};