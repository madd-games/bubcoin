/*
	Bubcoin

	Copyright (c) 2020, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef NET_H_
#define NET_H_

#undef WINVER
#undef _WIN32_WINNT
#define	WINVER					0x0601
#define	_WIN32_WINNT				0x0601

#include <inttypes.h>

// include OS-specific headers here
#ifdef __unix__
#	include <sys/socket.h>
#	include <netinet/in.h>
#	include <unistd.h>
#	include <netdb.h>
#	include <arpa/inet.h>
#	define	SOCK_INVALID			-1
#	define	closesock			close
typedef int socket_t;
#elif defined(_WIN32)
#	include <winsock2.h>
#	include <windows.h>
#	include <ws2tcpip.h>
#	include <ws2ipdef.h>
#	include <synchapi.h>
#	define	SOCK_INVALID			INVALID_SOCKET
#	define	closesock			closesocket
#	define	sleep(x)			Sleep((x) * 1000)
typedef SOCKET socket_t;
typedef int socklen_t;
#else
#	error "I don't recognise this OS!"
#endif

#include "block.h"

/**
 * The TCP port used for bubcoin.
 */
#define	BUBCOIN_PORT				25966
#define	BUBCOIN_PORT_AS_STRING			"25966"

/**
 * Resource types.
 */
#define	RES_ECHO				0
#define	RES_BLOCK_DATA				1
#define	RES_BLOCK_FOLLOWING			2
#define	RES_TRANSACTION				3
#define	RES_PEERS				4

/**
 * Error codes.
 */
#define	ERR_BLOCK_NOT_FOUND			1

/**
 * Magic header.
 */
typedef struct
{
	/**
	 * The string "BUB".
	 */
	char bub[3];

	/**
	 * Message type.
	 * 'Q' - query
	 * 'A' - answer/announcement
	 * 'E' - error (in response to query)
	 */
	char msgType;

	/**
	 * Resource type (for query and answer), or error code (for errors).
	 */
	union
	{
		uint8_t resType;
		uint8_t errNum;
	};
} MagicHeader;

/**
 * Initialize the networking system.
 */
void net_init();

/**
 * Announce a block to other nodes on the network.
 */
void net_announce_block(hash_t *hash, const void *block, size_t size);

/**
 * Announce a transaction to other nodes on the network.
 */
void net_announce_trans(const void *trans, size_t transSize);

/**
 * Get the current list of peers, and the count.
 */
void net_get_all_peers(struct in6_addr **out, int *outCount);

#endif
