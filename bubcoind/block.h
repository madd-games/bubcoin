/*
	Bubcoin

	Copyright (c) 2020, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef BLOCK_H_
#define BLOCK_H_

#include <stdlib.h>

#include "sig.h"
#include "sha256.h"
#include "thread.h"

#define	BLK_ENT_HEADER				0x00
#define	BLK_ENT_TRANSACTION			0x01
#define	BLK_ENT_FOOTER				0xFF

#define	FULL_BUBCOIN				1000000

#define	DESIRED_MINING_INTERVAL			2*60

/**
 * Block header entry.
 */
typedef struct
{
	uint8_t type;				/* BLK_ENT_HEADER */
	hash_t prevBlockHash;
	pubkey_t minerID;
	uint64_t timestamp;
} __attribute__ ((packed)) BlockHeaderEntry;

/**
 * Transaction entry.
 */
typedef struct
{
	uint8_t type;				/* BLK_ENT_TRANSACTION */
	pubkey_t spenderID;
	sig_t signature;
	uint32_t numInputs;
	uint32_t numOutputs;
	uint64_t timestamp;
} __attribute__ ((packed)) BlockTransactionEntryHeader;

/**
 * Transaction input.
 */
typedef struct
{
	hash_t inputBlockHash;
	uint64_t entryIndex;
} __attribute__ ((packed)) BlockTransactionEntryInput;

/**
 * Transaction output.
 */
typedef struct
{
	pubkey_t targetID;
	uint64_t value;
} __attribute__ ((packed)) BlockTransactionEntryOutput;

/**
 * Block footer entry.
 */
typedef struct
{
	uint8_t type;				/* BLK_ENT_FOOTER */
	uint64_t difficulty;
	uint64_t nonce;
} __attribute__ ((packed)) BlockFooterEntry;

/**
 * Block notes file format.
 */
typedef struct
{
	/**
	 * Total difficulty low and high. This it the difficulty of this block,
	 * plus the difficulty of all blocks that came before it.
	 */
	uint64_t difficultyLow;
	uint64_t difficultyHigh;
	
	/**
	 * Difficulty of the block itself (matches the value in the footer).
	 */
	uint64_t localDifficulty;
	
	/**
	 * Block number, 0 meaning the genesis block, followed by block 1 etc.
	 */
	uint64_t blockNum;
} BlockNotes;

/**
 * Check if the difficulty of a block is correct (check the hash).
 */
int block_is_difficulty_correct(const void *block, size_t size);

/**
 * Mine the specified block. The block must be fully valid (header, transactions, footer), but with
 * the nonce not yet set. This function calculates the nonce.
 */
int block_mine(void *block, size_t size);

/**
 * Initialize the block subsystem (e.g. loading the HEADS file).
 */
void block_init();

/**
 * Initialize the miner. Generate a keypair if one is not yet available.
 */
void block_init_miner();

/**
 * Add a block to the chain. Returns 0 if successful, -1 if not. Failure occurs if the block is invalid,
 * or if it can't be attached to any head.
 */
int block_add(void *block, size_t size);

/**
 * Get the "strongest head" hash (the head with the most total difficulty).
 */
void block_get_strongest_head(hash_t *hash);

/**
 * Mine the next block, and add it to the chain. Returns 0 if successful, -1 if not.
 */
int block_mine_next();

/**
 * Get the hash of the block preceding the block with the specified hash. Returns 0 on success,
 * or -1 if there was an error. The error is also reported to the log. The block_is_null() function
 * determines whether the hash is the null block; this function fails for the null block.
 * 
 * The following example shows how to iterate over a chain of blocks beginning at some hash:
 * 
 * 	hash_t hash;
 * 	for (get_first_hash(&hash); !block_is_null(&hash); block_get_prev(&hash))
 * 	{
 *		...
 *	}
 */
int block_get_prev(hash_t *hash);

/**
 * Returns nonzero if the specified hash is the "null block" (the block hypothetically preceding the genesis block).
 */
int block_is_null(hash_t *hash);

/**
 * Returns nonzero if the (spender/block/entry) tuple was ever used as an input in block 'scan' or before.
 */
int block_is_spent(hash_t *scan, pubkey_t *spender, hash_t *block, uint64_t entry);

/**
 * Returns the value of the input (spender/block/entry). Returns 0 if the tuple was already spent, or does not exist.
 */
uint64_t block_get_value(hash_t *scan, pubkey_t *spender, hash_t *block, uint64_t entry);

/**
 * Calculate how much money is currently owned by you (according to the most difficult branch).
 */
uint64_t block_get_my_balance();

/**
 * Create a transaction from me, to the specified target, of the specified amount. Returns 0 on success, or -1
 * if the funds were not found.
 */
int block_make_spend(uint64_t amount, pubkey_t *target, void **transOut, size_t *sizeOut);

/**
 * Read block data, and return an allocated buffer, and its size, in the given pointers. Returns 0 if successful,
 * -1 if the block could not be found.
 */
int block_read(hash_t *hash, void **dataOut, size_t *sizeOut);

/**
 * Atomically obtain the current list of heads.
 */
hash_t* block_get_all_heads(int *countOut);

/**
 * Check if the block of the specified hash already exists on our copy of the blockchain.
 */
int block_exists(hash_t *hash);

/**
 * Run the block mining thread.
 */
void block_start_miner_thread();

/**
 * Get our public key.
 */
void block_get_my_public_key(pubkey_t *key);

/**
 * Return the current block mining progress.
 */
uint64_t block_get_progress();

#endif