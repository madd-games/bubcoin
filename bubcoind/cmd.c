/*
	Bubcoin

	Copyright (c) 2020, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#include "cmd.h"
#include "net.h"
#include "block.h"
#include "log.h"
#include "ether.h"

#define	CMD_PORT				25967

static socket_t cmdSock;

uint64_t cmd_parse_bub(const char *bub)
{
	uint64_t quotient = 0;
	uint64_t part = 0;
	int sawPoint = 0;
	uint64_t divisor = 1;
	
	for (; *bub!=0; bub++)
	{
		if (*bub == '.')
		{
			if (sawPoint) return 0;
			else sawPoint = 1;
		}
		else
		{
			if (*bub < '0' || *bub > '9')
			{
				return 0;
			};
			
			uint64_t digit = (*bub) - '0';
			if (!sawPoint)
			{
				quotient = quotient * 10 + digit;
			}
			else
			{
				part = part * 10 + digit;
				divisor *= 10;
				
				if (divisor > 1000000)
				{
					return 0;
				};
			};
		};
	};
	
	while (divisor < 1000000)
	{
		part *= 10;
		divisor *= 10;
	};
	
	return (quotient * 1000000) + part;
};

static uint8_t cmd_parse_hexdigit(char c)
{
	if (c >= '0' && c <= '9')
	{
		return c - '0';
	}
	else if (c >= 'a' && c <= 'f')
	{
		return c - 'a' + 10;
	}
	else if (c >= 'A' && c <= 'F')
	{
		return c - 'A' + 10;
	}
	else
	{
		return 0xFF;
	};
};

static int cmd_parse_pubkey(pubkey_t *pubkey, const char *scan)
{
	size_t size = sizeof(pubkey_t);
	uint8_t *put = pubkey->data;
	
	while (size--)
	{
		uint8_t hi = cmd_parse_hexdigit(*scan++);
		if (hi == 0xFF) return -1;
		uint8_t lo = cmd_parse_hexdigit(*scan++);
		if (lo == 0xFF) return -1;
		
		*put++ = (hi << 4) | lo;
	};
	
	if (*scan == 0)
	{
		return 0;
	};
	
	return -1;
};

static void cmd_exec(const char *cmd, socket_t sock)
{
	if (strcmp(cmd, "pubkey") == 0)
	{
		pubkey_t key;
		block_get_my_public_key(&key);
		
		char result[512];
		uint8_t *scan = key.data;
		size_t size = sizeof(pubkey_t);
		
		char *put = result;
		while (size--)
		{
			sprintf(put, "%02hhx", *scan++);
			put += 2;
		};
		
		*put = 0;
		
		send(sock, result, strlen(result), 0);
	}
	else if (strcmp(cmd, "balance") == 0)
	{
		uint64_t balance = block_get_my_balance();
		char result[512];
		sprintf(result, "Balance: %d.%06d BUB", (int) (balance / FULL_BUBCOIN), (int) (balance % FULL_BUBCOIN));
		
		send(sock, result, strlen(result), 0);
	}
	else if (strcmp(cmd, "progress") == 0)
	{
		char result[512];
		sprintf(result, "%016" PRIx64, block_get_progress());
		send(sock, result, strlen(result), 0);
	}
	else if (memcmp(cmd, "send ", 5) == 0)
	{
		const char *scan = cmd + 5;
		char bub[512];
		char *put = bub;
		
		while (*scan != ' ' && *scan != 0)
		{
			*put++ = *scan++;
		};
		
		*put = 0;
		
		if (*scan == 0)
		{
			send(sock, "Invalid syntax", strlen("Invalid syntax"), 0);
			return;
		};
		
		scan++;		// skip over the space
		
		uint64_t amount = cmd_parse_bub(bub);
		if (amount == 0)
		{
			const char *result = "Invalid amount specified";
			send(sock, result, strlen(result), 0);
			return;
		};
		
		if (memcmp(scan, "to ", 3) != 0)
		{
			send(sock, "Invalid syntax", strlen("Invalid syntax"), 0);
			return;
		};
		
		scan += 3;
		
		pubkey_t targetID;
		if (cmd_parse_pubkey(&targetID, scan) != 0 || !sig_is_pubkey_valid(&targetID))
		{
			const char *result = "Invalid public key";
			send(sock, result, strlen(result), 0);
			return;
		};
		
		void *trans;
		size_t transSize;
		
		if (block_make_spend(amount, &targetID, &trans, &transSize) != 0)
		{
			const char *result = "Insufficient funds";
			send(sock, result, strlen(result), 0);
			return;
		};
		
		ether_add(trans, transSize);
		free(trans);
		send(sock, "OK", 2, 0);
	}
	else if (strcmp(cmd, "peers") == 0)
	{
		struct in6_addr *peers;
		int numPeers;
		net_get_all_peers(&peers, &numPeers);
		
		int i;
		for (i=0; i<numPeers; i++)
		{
			char addrAsStr[INET6_ADDRSTRLEN+1];
			inet_ntop(AF_INET6, &peers[i], addrAsStr, INET6_ADDRSTRLEN);
			strcat(addrAsStr, "\n");
			send(sock, addrAsStr, strlen(addrAsStr), 0);
		};
		
		free(peers);
	}
	else
	{
		const char *result = "Error: unknown command";
		send(sock, result, strlen(result), 0);
	};
};

static void cmd_thread(void *ignore)
{
	(void) ignore;
	
	bublog(LOG_INFO, "command-server started");
	
	while (1)
	{
		struct sockaddr_in6 addr;
		socklen_t addrlen = sizeof(struct sockaddr_in6);
		socket_t sock = accept(cmdSock, (struct sockaddr*) &addr, &addrlen);
		if (sock == -1) continue;
		if (!IN6_IS_ADDR_LOOPBACK(&addr.sin6_addr)) continue;
		
		char cmd[512];
		char *end = &cmd[510];
		char *put = cmd;
		
		int ok = 0;
		while (1)
		{
			if (put == end) break;
			
			if (recv(sock, put, 1, MSG_WAITALL) != 1)
			{
				break;
			};
			
			if (*put == 0)
			{
				ok = 1;
				break;
			};
			
			put++;
		};
		
		if (ok)
		{
			cmd_exec(cmd, sock);
		};
		
		closesock(sock);
	};
};

void cmd_start()
{
	socket_t sock = socket(AF_INET6, SOCK_STREAM, 0);
	if (sock == SOCK_INVALID)
	{
		bublog(LOG_ERROR, "failed to start command line server");
		bub_abort();
	};
	
	struct sockaddr_in6 sa;
	memset(&sa, 0, sizeof(struct sockaddr_in6));
	sa.sin6_family = AF_INET6;
	sa.sin6_addr = in6addr_loopback;
	sa.sin6_port = htons(CMD_PORT);
	
	if (bind(sock, (struct sockaddr*) &sa, sizeof(struct sockaddr_in6)) != 0)
	{
		bublog(LOG_ERROR, "failed to bind to command line port (%d)", CMD_PORT);
		bub_abort();
	};
	
	if (listen(sock, 5) != 0)
	{
		bublog(LOG_ERROR, "failed to set command-line socket to listening");
		bub_abort();
	};
	
	cmdSock = sock;
	thread_detach(thread_start(cmd_thread, NULL));
};
