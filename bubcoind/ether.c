/*
	Bubcoin

	Copyright (c) 2020, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <string.h>
#include <stddef.h>
#include <time.h>

#include "ether.h"
#include "thread.h"
#include "log.h"
#include "sha256.h"
#include "sig.h"
#include "block.h"
#include "net.h"

typedef struct
{
	pubkey_t spender;
	hash_t block;
	uint64_t entry;
} InputInfo;

static Mutex *mtxEther;
static EtherEntry *etherHead;

static void ether_thread(void *ignore)
{
	(void) ignore;
	
	while (1)
	{
		mutex_lock(mtxEther);
		
		EtherEntry **nextPtr = &etherHead;
		while (*nextPtr != NULL)
		{
			EtherEntry *ent = *nextPtr;
			if (ent->header->timestamp < time(NULL)-ETHER_TIMEOUT)
			{
				*nextPtr = ent->next;
				free(ent->trans);
				free(ent);
				
				bublog(LOG_DEBUG, "purged an expired transaction");
			}
			else
			{
				nextPtr = &ent->next;
			};
		};
		
		mutex_unlock(mtxEther);
		
		sleep(10);
	};
};

void ether_init()
{
	mtxEther = mutex_new();
	thread_detach(thread_start(ether_thread, NULL));
};

void ether_add(void *trans, size_t size)
{
	if (size < sizeof(BlockTransactionEntryHeader))
	{
		return;
	};
	
	BlockTransactionEntryHeader *header = (BlockTransactionEntryHeader*) trans;
	if (size != sizeof(BlockTransactionEntryHeader) + header->numInputs * sizeof(BlockTransactionEntryInput) + header->numOutputs * sizeof(BlockTransactionEntryOutput))
	{
		return;
	};
	
	if (header->timestamp < time(NULL)-ETHER_TIMEOUT)
	{
		return;
	};
	
	if (header->timestamp > time(NULL)+5)
	{
		return;
	};
	
	sha256_t ctx;
	sha256_init(&ctx);
	
	// feed everything up to the signature
	sha256_feed(&ctx, trans, offsetof(BlockTransactionEntryHeader, signature));
	
	// feed zeroes in place of the signature
	static sig_t zerosig;
	sha256_feed(&ctx, &zerosig, sizeof(sig_t));
	
	// now the rest
	sha256_feed(&ctx, &header->numInputs, size - offsetof(BlockTransactionEntryHeader, numInputs));
	
	// now verify
	if (!sig_is_valid(&header->spenderID, &ctx, &header->signature))
	{
		return;
	};
	
	mutex_lock(mtxEther);
	
	EtherEntry *ent;
	for (ent=etherHead; ent!=NULL; ent=ent->next)
	{
		if (ent->transSize == size)
		{
			if (memcmp(trans, ent->trans, size) == 0)
			{
				mutex_unlock(mtxEther);
				return;
			};
		};
	};
	
	ent = (EtherEntry*) malloc(sizeof(EtherEntry));
	ent->trans = malloc(size);
	ent->transSize = size;
	memcpy(ent->trans, trans, size);
	
	ent->header = (BlockTransactionEntryHeader*) ent->trans;
	ent->inputs = (BlockTransactionEntryInput*) ((char*) ent->trans + sizeof(BlockTransactionEntryHeader));
	ent->outputs = (BlockTransactionEntryOutput*) ((char*) ent->inputs + sizeof(BlockTransactionEntryInput) * ent->header->numInputs);
	
	ent->next = etherHead;
	etherHead = ent;
	
	uint32_t numInputs = ent->header->numInputs;
	uint32_t numOutputs = ent->header->numOutputs;
	mutex_unlock(mtxEther);
	
	bublog(LOG_INFO, "accepted a transaction into the ether: %d inputs, %d outputs", (int) numInputs, (int) numOutputs);
	net_announce_trans(trans, size);
};

void ether_select_for_block(void **blockInOut, size_t *sizeInOut)
{
	void *block = *blockInOut;
	size_t currentBlockSize = *sizeInOut;
	mutex_lock(mtxEther);
	
	BlockHeaderEntry *header = (BlockHeaderEntry*) block;
	uint64_t blockTimestamp = header->timestamp;
	hash_t prevBlockHash = header->prevBlockHash;
	
	size_t offset = sizeof(BlockHeaderEntry);
	
	int entriesAdmitted = 0;
	
	int numInputs = 0;
	InputInfo *currentInputs = NULL;
	
	EtherEntry *ent;
	for (ent=etherHead; ent!=NULL; ent=ent->next)
	{
		// check if the transaction timed out
		if (ent->header->timestamp < blockTimestamp-ETHER_TIMEOUT)
		{
			continue;
		};
		
		// check if any of its inputs are already admitted to this block
		int i;
		int alreadyHave = 0;
		for (i=0; i<numInputs; i++)
		{
			InputInfo *info = &currentInputs[i];
			
			int j;
			for (j=0; j<ent->header->numInputs; j++)
			{
				BlockTransactionEntryInput *input = &ent->inputs[j];
				
				if (memcmp(&info->spender, &ent->header->spenderID, sizeof(pubkey_t)) == 0
					&& memcmp(&info->block, &input->inputBlockHash, sizeof(hash_t)) == 0
					&& info->entry == input->entryIndex)
				{
					alreadyHave = 1;
					break;
				};
			};
			
			if (alreadyHave) break;
		};
		
		if (alreadyHave)
		{
			continue;
		};
		
		// nothing admitted to this block yet, but check that they were not otherwise spent,
		// and that they belong to the spender
		uint64_t spendValue = 0;
		for (i=0; i<ent->header->numInputs; i++)
		{
			BlockTransactionEntryInput *input = &ent->inputs[i];
			uint64_t value = block_get_value(&prevBlockHash, &ent->header->spenderID, &input->inputBlockHash, input->entryIndex);
			if (value == 0)
			{
				break;
			};
			
			spendValue += value;
		};
		
		if (i != ent->header->numInputs)
		{
			continue;
		};
		
		// check that the transaction is balanced
		uint64_t outputValue = 0;
		for (i=0; i<ent->header->numOutputs; i++)
		{
			outputValue += ent->outputs[i].value;
		};
		
		if (spendValue != outputValue)
		{
			continue;
		};
		
		// everything is good, add to this block
		int firstInfoIndex = numInputs;
		numInputs += ent->header->numInputs;
		
		currentInputs = (InputInfo*) realloc(currentInputs, sizeof(InputInfo) * numInputs);
		for (i=0; i<ent->header->numInputs; i++)
		{
			BlockTransactionEntryInput *input = &ent->inputs[i];
			currentInputs[firstInfoIndex+i].spender = ent->header->spenderID;
			currentInputs[firstInfoIndex+i].block = input->inputBlockHash;
			currentInputs[firstInfoIndex+i].entry = input->entryIndex;
		};
		
		currentBlockSize += ent->transSize;
		block = realloc(block, currentBlockSize);
		memcpy((char*) block + offset, ent->trans, ent->transSize);
		offset += ent->transSize;
		
		entriesAdmitted++;
	};
	
	mutex_unlock(mtxEther);
	*blockInOut = block;
	*sizeInOut = currentBlockSize;
	
	bublog(LOG_DEBUG, "%d transactions admitted to currently-mined block", entriesAdmitted);
};
