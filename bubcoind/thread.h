/*
	Bubcoin

	Copyright (c) 2020, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef THREAD_H_
#define THREAD_H_

/**
 * Represents a mutex.
 */
typedef struct Mutex_ Mutex;

/**
 * Represents a thread.
 */
typedef struct Thread_ Thread;

/**
 * Thread entry point function type.
 */
typedef void (*thread_entry_t)(void *param);

/**
 * Create a new mutex.
 */
Mutex* mutex_new();

/**
 * Destroy a mutex.
 */
void mutex_destroy(Mutex *mtx);

/**
 * Lock a mutex.
 */
void mutex_lock(Mutex *mtx);

/**
 * Unlock a mutex.
 */
void mutex_unlock(Mutex *mtx);

/**
 * Start a new thread, and within it, call 'entry' with argument 'param'.
 */
Thread* thread_start(thread_entry_t entry, void *param);

/**
 * Detach a thread.
 */
void thread_detach(Thread *th);

/**
 * Wait for a thread to terminate.
 */
void thread_wait(Thread *th);

/**
 * Exit from the current thread.
 */
void thread_exit();

#endif