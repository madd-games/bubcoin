/*
	Bubcoin

	Copyright (c) 2020, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SHA256_H_
#define SHA256_H_

#include <stdint.h>
#include <stdlib.h>

#define	HASH_STRING_SIZE		(sizeof(hash_t) * 2 + 1)

/**
 * SHA256 context.
 */
typedef struct
{
	uint32_t h[8];			/* internal state */
	uint64_t len;			/* current length in bytes */
	uint64_t buflen;		/* number of bytes currently in the buffer */
	uint8_t buffer[64];		/* the buffer */
} sha256_t;

/**
 * Hash value (block hash).
 */
typedef struct
{
	uint8_t bytes[32];
} hash_t;

/**
 * Initialize a SHA256 context. You may later call sha256_feed() to add more data to the message. After
 * all data was fed, call sha256_digest() to get the final hash.
 */
void sha256_init(sha256_t *ctx);

/**
 * Feed data into a SHA256 context.
 */
void sha256_feed(sha256_t *ctx, const void *data, size_t size);

/**
 * Read out the final SHA256 hash from a context. The buffer must be 32 bytes long.
 */
void sha256_digest(sha256_t *ctx, void *buffer);

/**
 * Produce a hash value from a SHA256 context (block hash).
 */
void sha256_to_hash(sha256_t *ctx, hash_t *hash);

/**
 * Convert a hash into a string. The buffer must be at least of size HASH_STRING_SIZE.
 */
void hash_to_string(hash_t *hash, char *buffer);

#endif