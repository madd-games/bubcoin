/*
	Bubcoin

	Copyright (c) 2020, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stddef.h>

#include "net.h"
#include "log.h"
#include "thread.h"
#include "block.h"
#include "sha256.h"
#include "ether.h"

static socket_t serverSocket;
static struct in6_addr *peers;
static int numPeers;
static Mutex *mtxPeers;

typedef struct
{
	struct sockaddr_in6 sa;
	hash_t hash;
} BlockFetchThreadInfo;

typedef struct
{
	void *block;
	size_t size;
} BlockBuffer;

typedef struct
{
	pubkey_t spender;
	hash_t inputBlock;
	uint64_t entryIndex;
} PrevInputInfo;

typedef struct
{
	size_t size;
	hash_t hash;
	char data[];
} BlockAnnounceThreadInfo;

typedef struct
{
	size_t size;
	char data[];
} TransAnnounceThreadInfo;

static void net_recv_block(socket_t sock, hash_t *expectedHash);

static int net_recv_to_buffer(socket_t sock, size_t size, BlockBuffer *buf)
{
	size_t offset = buf->size;
	buf->size += size;
	buf->block = realloc(buf->block, buf->size);

	if (recv(sock, (char*) buf->block + offset, size, MSG_WAITALL) != size)
	{
		return -1;
	};

	return 0;
};

static void net_save_peers()
{
	mutex_lock(mtxPeers);

	FILE *fp = fopen("peers.dat", "wb");
	if (fp == NULL)
	{
		bublog(LOG_ERROR, "failed to open peers.dat for writing");
	}
	else
	{
		if (fwrite(peers, sizeof(struct in6_addr), numPeers, fp) != numPeers)
		{
			remove("peers.dat");
			bublog(LOG_ERROR, "failed to write to peers.dat");
		};

		fclose(fp);
	};

	mutex_unlock(mtxPeers);
};

static void net_get_random_peer(struct in6_addr *result)
{
	mutex_lock(mtxPeers);

	int index = rand() % numPeers;
	memcpy(result, &peers[index], sizeof(struct in6_addr));

	mutex_unlock(mtxPeers);
};

static void admit_peer(struct in6_addr *addr)
{
	mutex_lock(mtxPeers);

	int i;
	for (i=0; i<numPeers; i++)
	{
		if (memcmp(&peers[i], addr, sizeof(struct in6_addr)) == 0)
		{
			mutex_unlock(mtxPeers);
			return;
		};
	};

	int index = numPeers++;
	peers = (struct in6_addr*) realloc(peers, sizeof(struct in6_addr) * numPeers);
	memcpy(&peers[index], addr, sizeof(struct in6_addr));

	mutex_unlock(mtxPeers);
	net_save_peers();

	char addrAsStr[INET6_ADDRSTRLEN];
	inet_ntop(AF_INET6, addr, addrAsStr, INET6_ADDRSTRLEN);
	bublog(LOG_INFO, "admitted new peer: %s", addrAsStr);
};

static void net_send_error(socket_t sock, uint8_t errNum)
{
	MagicHeader resp;
	memcpy(resp.bub, "BUB", 3);
	resp.msgType = 'E';
	resp.errNum = errNum;
	send(sock, (const char*) &resp, sizeof(MagicHeader), 0);
};

static void net_admit_peer_by_sock(socket_t sock)
{
	struct sockaddr_in6 sa;
	socklen_t socklen = sizeof(sa);
	getpeername(sock, (struct sockaddr*) &sa, &socklen);

	admit_peer(&sa.sin6_addr);
};

static void net_handle_query(socket_t sock, uint8_t resType)
{
	if (resType == RES_ECHO)
	{
		MagicHeader resp;
		memcpy(resp.bub, "BUB", 3);
		resp.msgType = 'A';
		resp.resType = RES_ECHO;

		net_admit_peer_by_sock(sock);

		send(sock, (const char*) &resp, sizeof(MagicHeader), 0);
		closesock(sock);
	}
	else if (resType == RES_BLOCK_DATA)
	{
		hash_t blockHash;
		if (recv(sock, (char*) &blockHash, sizeof(hash_t), MSG_WAITALL) != sizeof(hash_t))
		{
			bublog(LOG_DEBUG, "net_handle_query: did not receive block hash");
			closesock(sock);
			return;
		};

		void *block;
		size_t size;
		if (block_read(&blockHash, &block, &size) != 0)
		{
			char hexstr[256];
			hash_to_string(&blockHash, hexstr);
			bublog(LOG_DEBUG, "net_handle_query: cannot find requested block %s", hexstr);

			net_send_error(sock, ERR_BLOCK_NOT_FOUND);
			closesock(sock);
			return;
		};

		net_admit_peer_by_sock(sock);

		MagicHeader header;
		memcpy(header.bub, "BUB", 3);
		header.msgType = 'A';
		header.resType = RES_BLOCK_DATA;
		if (send(sock, (const char*) &header, sizeof(MagicHeader), 0) != sizeof(MagicHeader))
		{
			bublog(LOG_DEBUG, "net_handle_query: failed to send response header");
			closesock(sock);
			free(block);
			return;
		};

		if (send(sock, (const char*) &blockHash, sizeof(hash_t), 0) != sizeof(hash_t))
		{
			bublog(LOG_DEBUG, "net_handle_query: failed to send block hash");
			closesock(sock);
			free(block);
			return;
		};

		if (send(sock, (const char*) block, size, 0) != size)
		{
			bublog(LOG_DEBUG, "net_handle_query: failed to send full block");
		};

		closesock(sock);
		free(block);
	}
	else if (resType == RES_BLOCK_FOLLOWING)
	{
		uint8_t numHashes;
		if (recv(sock, (char*) &numHashes, 1, MSG_WAITALL) != 1)
		{
			bublog(LOG_DEBUG, "net_handle_query: incomplete RES_BLOCK_FOLLOWING query");
			closesock(sock);
			return;
		};

		if (numHashes > 16)
		{
			bublog(LOG_DEBUG, "net_handle_query: RES_BLOCK_FOLLOWING query with too many heads");
			closesock(sock);
			return;
		};

		hash_t peerHashes[16];
		if (recv(sock, (char*) peerHashes, sizeof(hash_t) * numHashes, MSG_WAITALL) != sizeof(hash_t) * numHashes)
		{
			bublog(LOG_DEBUG, "net_handle_query: incomplete RES_BLOCK_FOLLOWING query");
			closesock(sock);
			return;
		};

		uint8_t myRespCount = 0;
		hash_t myResps[64];

		int numHeads;
		hash_t *heads = block_get_all_heads(&numHeads);

		int i;
		for (i=0; i<numHashes; i++)
		{
			if (block_is_null(&peerHashes[i]))
			{
				continue;
			};

			if (myRespCount == 64)
			{
				break;
			};

			int j;
			for (j=0; j<numHeads; j++)
			{
				if (myRespCount == 64)
				{
					break;
				};

				if (memcmp(&heads[j], &peerHashes[i], sizeof(hash_t)) == 0)
				{
					// this is one of our heads, so we don't have anything before it
					continue;
				};

				hash_t nextHash = heads[j];
				hash_t currentHash = nextHash;
				block_get_prev(&currentHash);

				while (!block_is_null(&currentHash) && memcmp(&peerHashes[i], &currentHash, sizeof(hash_t)) != 0)
				{
					nextHash = currentHash;
					block_get_prev(&currentHash);
				};

				if (!block_is_null(&currentHash))
				{
					int k;
					for (k=0; k<myRespCount; k++)
					{
						if (memcmp(&myResps[k], &nextHash, sizeof(hash_t)) == 0)
						{
							break;
						};
					};

					int l;
					for (l=0; l<numHashes; l++)
					{
						if (memcmp(&peerHashes[l], &nextHash, sizeof(hash_t)) == 0)
						{
							break;
						};
					};

					if (k == myRespCount && l == numHashes)
					{
						uint8_t newIndex = myRespCount++;
						memcpy(&myResps[newIndex], &nextHash, sizeof(hash_t));
					};
				};
			};
		};

		free(heads);

		MagicHeader resphead;
		memcpy(resphead.bub, "BUB", 3);
		resphead.msgType = 'A';
		resphead.resType = RES_BLOCK_FOLLOWING;

		if (send(sock, (const char*) &resphead, sizeof(MagicHeader), 0) != sizeof(MagicHeader))
		{
			bublog(LOG_DEBUG, "failed to send RES_BLOCK_FOLLOWING header");
			closesock(sock);
			return;
		};

		if (send(sock, (const char*) &myRespCount, 1, 0) != 1)
		{
			bublog(LOG_DEBUG, "failed to send myRespCount in RES_BLOCK_FOLLOWING response");
			closesock(sock);
			return;
		};

		if (myRespCount != 0)
		{
			if (send(sock, (const char*) myResps, sizeof(hash_t) * myRespCount, 0) != sizeof(hash_t) * myRespCount)
			{
				bublog(LOG_DEBUG, "failed to send hashes in RES_BLOCK_FOLLOWING response");
			};
		};

		net_admit_peer_by_sock(sock);
		closesock(sock);
	}
	else if (resType == RES_PEERS)
	{
		MagicHeader resphead;
		memcpy(resphead.bub, "BUB", 3);
		resphead.msgType = 'A';
		resphead.resType = RES_PEERS;
		
		if (send(sock, (const char*) &resphead, sizeof(MagicHeader), 0) != sizeof(MagicHeader))
		{
			bublog(LOG_DEBUG, "failed to send RES_PEERS header");
			closesock(sock);
			return;
		};
		
		int i;
		for (i=0; i<8; i++)
		{
			struct in6_addr peer;
			net_get_random_peer(&peer);
			assert(sizeof(struct in6_addr) == 16);
			
			if (send(sock, (const char*) &peer, sizeof(struct in6_addr), 0) != sizeof(struct in6_addr))
			{
				bublog(LOG_DEBUG, "failed to send peer address");
				closesock(sock);
				return;
			};
		};
		
		closesock(sock);
	}
	else
	{
		bublog(LOG_DEBUG, "net_handle_query: unknown resource type %hhd\n", resType);
		closesock(sock);
	};
};

static void net_handle_announcement(socket_t sock, uint8_t resType)
{
	if (resType == RES_BLOCK_DATA)
	{
		hash_t hash;
		if (recv(sock, (char*) &hash, sizeof(hash_t), MSG_WAITALL) != sizeof(hash_t))
		{
			bublog(LOG_DEBUG, "net_handle_announcement: failed to receive hash for RES_BLOCK_DATA");
			closesock(sock);
			return;
		};

		net_recv_block(sock, &hash);
		net_admit_peer_by_sock(sock);
		closesock(sock);
	}
	else if (resType == RES_TRANSACTION)
	{
		BlockBuffer buf = {NULL, 0};

		// receive and verify the transaction header
		if (net_recv_to_buffer(sock, sizeof(BlockTransactionEntryHeader), &buf) != 0)
		{
			bublog(LOG_ERROR, "net_handle_announcement: failed to receive transaction header");
			free(buf.block);
			return;
		};

		BlockTransactionEntryHeader *header = (BlockTransactionEntryHeader*) buf.block;
		size_t transSize = sizeof(BlockTransactionEntryHeader)
					+ header->numInputs * sizeof(BlockTransactionEntryInput)
					+ header->numOutputs * sizeof(BlockTransactionEntryOutput);

		if (net_recv_to_buffer(sock, transSize - sizeof(BlockTransactionEntryHeader), &buf) != 0)
		{
			bublog(LOG_ERROR, "net_handle_announcement: failed to receive transaction");
			free(buf.block);
			return;
		};

		ether_add(buf.block, transSize);
		free(buf.block);
		closesock(sock);
	}
	else
	{
		bublog(LOG_DEBUG, "net_handle_announcement: unknown resource type %hhd\n", resType);
		closesock(sock);
	};
};

static void net_request_thread(void *data)
{
	socket_t *sptr = (socket_t*) data;
	socket_t sock = *sptr;
	free(sptr);

	MagicHeader header;
	if (recv(sock, (char*) &header, sizeof(MagicHeader), MSG_WAITALL) != sizeof(MagicHeader))
	{
		bublog(LOG_DEBUG, "net_request_thread: failed to receive full header");
		closesock(sock);
		return;
	};

	if (memcmp(header.bub, "BUB", 3) != 0)
	{
		bublog(LOG_DEBUG, "net_request_thread: bad magic value in header");
		closesock(sock);
		return;
	};

	if (header.msgType == 'Q')
	{
		net_handle_query(sock, header.resType);
	}
	else if (header.msgType == 'A')
	{
		net_handle_announcement(sock, header.resType);
	}
	else
	{
		bublog(LOG_DEBUG, "net_request_thread: invalid message type %c", header.msgType);
		closesock(sock);
	};
};

static void net_server_thread(void *ignore)
{
	(void)ignore;
	socket_t sock = serverSocket;

	bublog(LOG_INFO, "bubcoin server started");

	while (1)
	{
		socket_t client = accept(sock, NULL, NULL);
		if (client == -1)
		{
			bublog(LOG_ERROR, "accept() failed on server socket");
			continue;
		};

		socket_t *passThrough = (socket_t*) malloc(sizeof(socket_t));
		*passThrough = client;
		Thread *th = thread_start(net_request_thread, passThrough);
		thread_detach(th);
	};
};

static void net_recv_block(socket_t sock, hash_t *expectedHash)
{
	BlockBuffer buf = {NULL, 0};
	char hashAsString[HASH_STRING_SIZE];
	hash_to_string(expectedHash, hashAsString);

	// receive and verify the block header
	if (net_recv_to_buffer(sock, sizeof(BlockHeaderEntry), &buf) != 0)
	{
		bublog(LOG_ERROR, "net_recv_block: failed to receive header");
		free(buf.block);
		return;
	};

	BlockHeaderEntry *header = (BlockHeaderEntry*) buf.block;
	if (header->type != BLK_ENT_HEADER)
	{
		bublog(LOG_ERROR, "net_recv_block: invalid header: wrong type");
		free(buf.block);
		return;
	};

	int numHeads;
	hash_t *heads = block_get_all_heads(&numHeads);

	int i;
	for (i=0; i<numHeads; i++)
	{
		if (memcmp(&heads[i], &header->prevBlockHash, sizeof(hash_t)) == 0)
		{
			break;
		};
	};

	free(heads);
	if (i == numHeads)
	{
		bublog(LOG_ERROR, "net_recv_block: received block does not belong to any head");
		free(buf.block);
		return;
	};

	hash_t prevBlockHash = header->prevBlockHash;

	uint64_t blockTimestamp = header->timestamp;
	if (blockTimestamp >= time(NULL)+5)		// give 5 seconds of leeway in case of unsynced clocks
	{
		bublog(LOG_ERROR, "net_recv_block: received a time-travelling block (%s)", hashAsString);
		free(buf.block);
		return;
	};

	PrevInputInfo *prevInputs = NULL;
	uint64_t numPrevInputs = 0;

	// receive and verify transactions
	while (1)
	{
		size_t offset = buf.size;
		if (net_recv_to_buffer(sock, 1, &buf) != 0)
		{
			bublog(LOG_ERROR, "net_recv_block: premature end of block");
			free(buf.block);
			free(prevInputs);
			return;
		};

		uint8_t entType = *((uint8_t*) buf.block + offset);
		if (entType == BLK_ENT_TRANSACTION)
		{
			// receive the rest of the header
			if (net_recv_to_buffer(sock, sizeof(BlockTransactionEntryHeader) - 1, &buf) != 0)
			{
				bublog(LOG_ERROR, "net_recv_block: failed to receive transaction header");
				free(buf.block);
				free(prevInputs);
				return;
			};

			BlockTransactionEntryHeader *header = (BlockTransactionEntryHeader*) ((char*) buf.block + offset);
			if (header->timestamp > blockTimestamp || header->timestamp < blockTimestamp-ETHER_TIMEOUT)
			{
				bublog(LOG_ERROR, "net_recv_block: block contains a timed-out transaction");
				free(buf.block);
				free(prevInputs);
				return;
			};

			pubkey_t spender = header->spenderID;
			uint32_t numInputs = header->numInputs;
			uint32_t numOutputs = header->numOutputs;
			size_t transSize = sizeof(BlockTransactionEntryHeader)
						+ numInputs * sizeof(BlockTransactionEntryInput)
						+ numOutputs * sizeof(BlockTransactionEntryOutput);

			if (numOutputs > numInputs*2)
			{
				bublog(LOG_ERROR, "net_recv_block: too many outputs in a transaction");
				free(buf.block);
				free(prevInputs);
				return;
			};

			uint64_t spendValue = 0;
			while (numInputs--)
			{
				size_t inputOffset = buf.size;
				if (net_recv_to_buffer(sock, sizeof(BlockTransactionEntryInput), &buf) != 0)
				{
					bublog(LOG_ERROR, "net_recv_block: premature end of transaction");
					free(buf.block);
					free(prevInputs);
					return;
				};

				BlockTransactionEntryInput *input = (BlockTransactionEntryInput*) ((char*) buf.block + inputOffset);
				uint64_t i;
				for (i=0; i<numPrevInputs; i++)
				{
					if (memcmp(&prevInputs[i].spender, &spender, sizeof(pubkey_t)) == 0
						&& memcmp(&prevInputs[i].inputBlock, &input->inputBlockHash, sizeof(hash_t)) == 0
						&& prevInputs[i].entryIndex == input->entryIndex)
					{
						bublog(LOG_ERROR, "net_recv_block: block contains double-spend");
						free(buf.block);
						free(prevInputs);
						return;
					};
				};

				uint64_t thisValue = block_get_value(&prevBlockHash, &spender, &input->inputBlockHash, input->entryIndex);
				if (thisValue == 0)
				{
					bublog(LOG_ERROR, "net_recv_block: block contains double-spend");
					free(buf.block);
					free(prevInputs);
					return;
				};

				prevInputs = (PrevInputInfo*) realloc(prevInputs, sizeof(PrevInputInfo) * (++numPrevInputs));
				memcpy(&prevInputs[i].spender, &spender, sizeof(pubkey_t));
				memcpy(&prevInputs[i].inputBlock, &input->inputBlockHash, sizeof(hash_t));
				prevInputs[i].entryIndex = input->entryIndex;

				spendValue += thisValue;
			};

			uint64_t spendOutput = 0;
			size_t outputArrayOffset = buf.size;

			if (net_recv_to_buffer(sock, sizeof(BlockTransactionEntryOutput) * numOutputs, &buf) != 0)
			{
				bublog(LOG_ERROR, "net_recv_block: premature end of transaction");
				free(buf.block);
				free(prevInputs);
				return;
			};

			BlockTransactionEntryOutput *outputs = (BlockTransactionEntryOutput*) ((char*) buf.block + outputArrayOffset);
			for (i=0; i<numOutputs; i++)
			{
				if (outputs[i].value == 0)
				{
					bublog(LOG_ERROR, "net_recv_block: block contains a transaction with an output of zero");
					free(buf.block);
					free(prevInputs);
					return;
				};

				spendOutput += outputs[i].value;

				uint64_t j;
				for (j=0; j<numOutputs; j++)
				{
					if (i != j)
					{
						if (memcmp(&outputs[i].targetID, &outputs[j].targetID, sizeof(pubkey_t)) == 0)
						{
							bublog(LOG_ERROR, "net_recv_block: multiple outputs to the same target in a transaction");
							free(buf.block);
							free(prevInputs);
							return;
						};
					};
				};
			};

			if (spendOutput != spendValue)
			{
				bublog(LOG_ERROR, "net_recv_block: block contains an unbalanced transaction");
				free(buf.block);
				free(prevInputs);
				return;
			};

			// verify the signature
			char *data = (char*) buf.block + offset;
			sha256_t ctx;
			sha256_init(&ctx);

			header = (BlockTransactionEntryHeader*) data;

			// feed everything up to signature
			sha256_feed(&ctx, data, offsetof(BlockTransactionEntryHeader, signature));

			// feed zeroes in place of the signature
			static sig_t zerosig;
			sha256_feed(&ctx, &zerosig, sizeof(sig_t));

			// feed the rest
			sha256_feed(&ctx, &header->numInputs, transSize - offsetof(BlockTransactionEntryHeader, numInputs));

			// check
			if (!sig_is_valid(&header->spenderID, &ctx, &header->signature))
			{
				bublog(LOG_ERROR, "net_recv_block: transaction signature invalid");
				free(buf.block);
				free(prevInputs);
				return;
			};
		}
		else if (entType == BLK_ENT_FOOTER)
		{
			break;
		}
		else
		{
			bublog(LOG_ERROR, "net_recv_block: invalid entry type in block");
			free(buf.block);
			return;
		};
	};

	free(prevInputs);

	// receive the rest of the footer
	if (net_recv_to_buffer(sock, sizeof(BlockFooterEntry) - 1, &buf) != 0)
	{
		bublog(LOG_ERROR, "net_recv_block: premature end of footer");
		free(buf.block);
		return;
	};

	// verify the hash is as expected
	sha256_t ctx;
	sha256_init(&ctx);
	sha256_feed(&ctx, buf.block, buf.size);

	hash_t actualHash;
	sha256_to_hash(&ctx, &actualHash);

	if (memcmp(&actualHash, expectedHash, sizeof(hash_t)) != 0)
	{
		bublog(LOG_ERROR, "net_recv_block: block hash is wrong");
		free(buf.block);
		return;
	};

	if (!block_is_difficulty_correct(buf.block, buf.size))
	{
		bublog(LOG_ERROR, "net_recv_block: block hash does not match difficulty");
		free(buf.block);
		return;
	};

	block_add(buf.block, buf.size);
	free(buf.block);
};

static void block_fetch_thread(void *data)
{
	BlockFetchThreadInfo *info = (BlockFetchThreadInfo*) data;

	if (block_exists(&info->hash))
	{
		// already exists
		return;
	};

	socket_t sock = socket(AF_INET6, SOCK_STREAM, 0);
	if (sock == SOCK_INVALID)
	{
		bublog(LOG_ERROR, "block_fetch_thread: failed to create the socket");
		return;
	};

	int v6only = 0;
	if (setsockopt(sock, IPPROTO_IPV6, IPV6_V6ONLY, (const char*) &v6only, sizeof(v6only)) != 0)
	{
		bublog(LOG_ERROR, "block_fetch_thread: failed to disable IPv6-only on the socket");
		closesock(sock);
		return;
	};

	char addrAsStr[INET6_ADDRSTRLEN];
	inet_ntop(AF_INET6, &info->sa.sin6_addr, addrAsStr, INET6_ADDRSTRLEN);

	if (connect(sock, (struct sockaddr*) &info->sa, sizeof(struct sockaddr_in6)) != 0)
	{
		bublog(LOG_DEBUG, "block_fetch_thread: failed to connect to %s", addrAsStr);
		closesock(sock);
		return;
	};

	char hashAsStr[HASH_STRING_SIZE];
	hash_to_string(&info->hash, hashAsStr);

	MagicHeader reqhead;
	memcpy(reqhead.bub, "BUB", 3);
	reqhead.msgType = 'Q';
	reqhead.resType = RES_BLOCK_DATA;

	if (send(sock, (const char*) &reqhead, sizeof(MagicHeader), 0) != sizeof(MagicHeader))
	{
		bublog(LOG_ERROR, "block_fetch_thread: failed to send RES_BLOCK_DATA request to %s", addrAsStr);
		closesock(sock);
		return;
	};

	if (send(sock, (const char*) &info->hash, sizeof(hash_t), 0) != sizeof(hash_t))
	{
		bublog(LOG_ERROR, "block_fetch_thread: failed to send RES_BLOCK_DATA hash to %s", addrAsStr);
		closesock(sock);
		return;
	};

	MagicHeader resphead;
	if (recv(sock, (char*) &resphead, sizeof(MagicHeader), MSG_WAITALL) != sizeof(MagicHeader))
	{
		bublog(LOG_ERROR, "block_fetch_thread: failed to receive response header from %s", addrAsStr);
		closesock(sock);
		return;
	};

	if (memcmp(resphead.bub, "BUB", 3) != 0 || resphead.msgType != 'A' || resphead.resType != RES_BLOCK_DATA)
	{
		bublog(LOG_ERROR, "block_fetch_thread: %s sent an invalid response", addrAsStr);
		closesock(sock);
		return;
	};

	hash_t resphash;
	if (recv(sock, (char*) &resphash, sizeof(hash_t), MSG_WAITALL) != sizeof(hash_t))
	{
		bublog(LOG_ERROR, "block_fetch_thread: failed to receive hash from %s", addrAsStr);
		closesock(sock);
		return;
	};

	if (memcmp(&resphash, &info->hash, sizeof(hash_t)) != 0)
	{
		char respAsStr[HASH_STRING_SIZE];
		hash_to_string(&resphash, respAsStr);

		bublog(LOG_ERROR, "block_fetch_thread: %s replied with the wrong hash (expected %s, got %s)", addrAsStr, hashAsStr, respAsStr);
		closesock(sock);
		return;
	};

	net_recv_block(sock, &resphash);
	closesock(sock);
};

static void net_query_blocks_instance(void *ignore)
{
	(void) ignore;

	struct sockaddr_in6 sa;
	memset(&sa, 0, sizeof(struct sockaddr_in6));
	sa.sin6_family = AF_INET6;
	net_get_random_peer(&sa.sin6_addr);
	sa.sin6_port = htons(BUBCOIN_PORT);

	socket_t sock = socket(AF_INET6, SOCK_STREAM, 0);
	if (sock == SOCK_INVALID)
	{
		bublog(LOG_ERROR, "net_query_blocks_instance: failed to create socket");
		closesock(sock);
		return;
	};

	int v6only = 0;
	if (setsockopt(sock, IPPROTO_IPV6, IPV6_V6ONLY, (const char*) &v6only, sizeof(v6only)) != 0)
	{
		bublog(LOG_ERROR, "net_query_blocks_instance: failed to disable IPv6-only on the socket");
		closesock(sock);
		return;
	};

	char addrAsStr[INET6_ADDRSTRLEN];
	inet_ntop(AF_INET6, &sa.sin6_addr, addrAsStr, INET6_ADDRSTRLEN);

	// TODO: timeout
	if (connect(sock, (struct sockaddr*) &sa, sizeof(struct sockaddr_in6)) != 0)
	{
		bublog(LOG_DEBUG, "net_query_blocks_instance: failed to connect to %s", addrAsStr);
		closesock(sock);
		return;
	};

	int numHeads;
	hash_t *heads = block_get_all_heads(&numHeads);

	uint8_t numHeadsToSend = numHeads;
	if (numHeadsToSend > 16) numHeadsToSend = 16;

	MagicHeader header;
	memcpy(header.bub, "BUB", 3);
	header.msgType = 'Q';
	header.resType = RES_BLOCK_FOLLOWING;

	if (send(sock, (const char*) &header, sizeof(MagicHeader), 0) != sizeof(MagicHeader))
	{
		bublog(LOG_ERROR, "net_query_blocks_instance: failed to send magic header to %s", addrAsStr);
		closesock(sock);
		free(heads);
		return;
	};

	if (send(sock, (const char*) &numHeadsToSend, 1, 0) != 1)
	{
		bublog(LOG_ERROR, "net_query_blocks_instance: failed to send head count to %s", addrAsStr);
		closesock(sock);
		free(heads);
		return;
	};

	if (send(sock, (const char*) heads, sizeof(hash_t) * numHeadsToSend, 0) != sizeof(hash_t) * numHeadsToSend)
	{
		bublog(LOG_ERROR, "net_query_blocks_instance: failed to send heads to %s", addrAsStr);
		closesock(sock);
		free(heads);
		return;
	};

	free(heads);

	MagicHeader resphead;
	if (recv(sock, (char*) &resphead, sizeof(MagicHeader), MSG_WAITALL) != sizeof(MagicHeader))
	{
		bublog(LOG_ERROR, "net_query_blocks_instance: failed to receive response from %s", addrAsStr);
		closesock(sock);
		return;
	};

	if (memcmp(resphead.bub, "BUB", 3) != 0 || resphead.msgType != 'A' || resphead.resType != RES_BLOCK_FOLLOWING)
	{
		bublog(LOG_ERROR, "net_query_blocks_instance: invalid response header from %s", addrAsStr);
		closesock(sock);
		return;
	};

	uint8_t numRespHashes;
	if (recv(sock, (char*) &numRespHashes, 1, MSG_WAITALL) != 1)
	{
		bublog(LOG_ERROR, "net_query_blocks_instance: failed to receive hash count from %s", addrAsStr);
		closesock(sock);
		return;
	};

	if (numRespHashes > 64) numRespHashes = 64;
	if (numRespHashes == 0)
	{
		// no blocks to be received
		closesock(sock);
		return;
	};

	hash_t respHashes[64];
	if (recv(sock, (char*) respHashes, sizeof(hash_t) * numRespHashes, MSG_WAITALL) != sizeof(hash_t) * numRespHashes)
	{
		bublog(LOG_ERROR, "net_query_blocks_instance: failed to receive response hashes from %s", addrAsStr);
		closesock(sock);
		return;
	};

	closesock(sock);

	bublog(LOG_INFO, "net_query_blocks_instance: received %hhu proposed hashes from %s", numRespHashes, addrAsStr);

	BlockFetchThreadInfo thinfo[64];
	Thread* threads[64];

	int i;
	for (i=0; i<numRespHashes; i++)
	{
		memcpy(&thinfo[i].sa, &sa, sizeof(struct sockaddr_in6));
		memcpy(&thinfo[i].hash, &respHashes[i], sizeof(hash_t));
		threads[i] = thread_start(block_fetch_thread, &thinfo[i]);
	};

	for (i=0; i<numRespHashes; i++)
	{
		thread_wait(threads[i]);
	};
};

static void net_query_peers_instance(void *ignore)
{
	(void) ignore;

	struct sockaddr_in6 sa;
	memset(&sa, 0, sizeof(struct sockaddr_in6));
	sa.sin6_family = AF_INET6;
	net_get_random_peer(&sa.sin6_addr);
	sa.sin6_port = htons(BUBCOIN_PORT);

	socket_t sock = socket(AF_INET6, SOCK_STREAM, 0);
	if (sock == SOCK_INVALID)
	{
		bublog(LOG_ERROR, "net_query_peers_instance: failed to create socket");
		closesock(sock);
		return;
	};

	int v6only = 0;
	if (setsockopt(sock, IPPROTO_IPV6, IPV6_V6ONLY, (const char*) &v6only, sizeof(v6only)) != 0)
	{
		bublog(LOG_ERROR, "net_query_peers_instance: failed to disable IPv6-only on the socket");
		closesock(sock);
		return;
	};

	char addrAsStr[INET6_ADDRSTRLEN];
	inet_ntop(AF_INET6, &sa.sin6_addr, addrAsStr, INET6_ADDRSTRLEN);

	// TODO: timeout
	if (connect(sock, (struct sockaddr*) &sa, sizeof(struct sockaddr_in6)) != 0)
	{
		bublog(LOG_DEBUG, "net_query_peers_instance: failed to connect to %s", addrAsStr);
		closesock(sock);
		return;
	};
	
	MagicHeader head;
	memcpy(head.bub, "BUB", 3);
	head.msgType = 'Q';
	head.resType = RES_PEERS;
	
	if (send(sock, (const char*) &head, sizeof(MagicHeader), 0) != sizeof(MagicHeader))
	{
		bublog(LOG_DEBUG, "net_query_peers_instance: failed to send header to %s", addrAsStr);
		closesock(sock);
		return;
	};
	
	if (recv(sock, (char*) &head, sizeof(MagicHeader), MSG_WAITALL) != sizeof(MagicHeader))
	{
		bublog(LOG_DEBUG, "net_query_peers_instance: failed to receive header from %s", addrAsStr);
		closesock(sock);
		return;
	};
	
	if (memcmp(head.bub, "BUB", 3) != 0 || head.msgType != 'A' || head.resType != RES_PEERS)
	{
		bublog(LOG_DEBUG, "net_query_peers_instance: received an invalid header from %s", addrAsStr);
		closesock(sock);
		return;
	};
	
	struct in6_addr proposed[8];
	if (recv(sock, (char*) proposed, sizeof(proposed), MSG_WAITALL) != sizeof(proposed))
	{
		bublog(LOG_DEBUG, "net_query_peers_instance: failed to receive peers from %s", addrAsStr);
		closesock(sock);
		return;
	};
	
	closesock(sock);
	
	int index = rand() % 8;
	memcpy(&sa.sin6_addr, &proposed[index], sizeof(struct in6_addr));
	
	sock = socket(AF_INET6, SOCK_STREAM, 0);
	if (sock == SOCK_INVALID)
	{
		bublog(LOG_ERROR, "net_query_peers_instance: failed to create socket");
		closesock(sock);
		return;
	};

	if (setsockopt(sock, IPPROTO_IPV6, IPV6_V6ONLY, (const char*) &v6only, sizeof(v6only)) != 0)
	{
		bublog(LOG_ERROR, "net_query_peers_instance: failed to disable IPv6-only on the socket");
		closesock(sock);
		return;
	};

	inet_ntop(AF_INET6, &sa.sin6_addr, addrAsStr, INET6_ADDRSTRLEN);

	// TODO: timeout
	if (connect(sock, (struct sockaddr*) &sa, sizeof(struct sockaddr_in6)) != 0)
	{
		bublog(LOG_DEBUG, "net_query_peers_instance: failed to connect to %s", addrAsStr);
		closesock(sock);
		return;
	};
	
	memcpy(head.bub, "BUB", 3);
	head.msgType = 'Q';
	head.resType = RES_ECHO;
	
	if (send(sock, (const char*) &head, sizeof(MagicHeader), 0) != sizeof(MagicHeader))
	{
		bublog(LOG_DEBUG, "net_query_peers_instance: failed to send echo header to %s", addrAsStr);
		closesock(sock);
		return;
	};
	
	if (recv(sock, (char*) &head, sizeof(MagicHeader), MSG_WAITALL) != sizeof(MagicHeader))
	{
		bublog(LOG_DEBUG, "net_query_peers_instance: failed to receive echo header from %s", addrAsStr);
		closesock(sock);
		return;
	};
	
	if (memcmp(head.bub, "BUB", 3) != 0 || head.msgType != 'A' || head.resType != RES_ECHO)
	{
		bublog(LOG_DEBUG, "net_query_peers_instance: invalid echo response from %s", addrAsStr);
		closesock(sock);
		return;
	};
	
	net_admit_peer_by_sock(sock);
	closesock(sock);
};

static void net_query_thread(void *ignore)
{
	(void) ignore;

	srand(time(NULL));
	while (1)
	{
		sleep(1 + rand() % 3);
		
		thread_detach(thread_start(net_query_blocks_instance, NULL));
		thread_detach(thread_start(net_query_peers_instance, NULL));
	};
};

void net_init()
{
#ifdef _WIN32
	WORD wVersionRequired = MAKEWORD(2, 2);
	WSADATA wsaData;

	if (WSAStartup(wVersionRequired, &wsaData) != 0)
	{
		bublog(LOG_ERROR, "WSAStartup failed");
		bub_abort();
	};
#endif

	FILE *fp = fopen("peers.dat", "rb");
	if (fp != NULL)
	{
		peers = (struct in6_addr*) malloc(sizeof(struct in6_addr) * 64);
		numPeers = fread(peers, sizeof(struct in6_addr), 64, fp);
		fclose(fp);
	};

	if (numPeers == 0)
	{
		free(peers);
		peers = NULL;

		struct addrinfo hints;
		memset(&hints, 0, sizeof(struct addrinfo));
		hints.ai_family = AF_INET6;
		hints.ai_socktype = SOCK_STREAM;
		hints.ai_protocol = 0;
		hints.ai_flags = AI_V4MAPPED | AI_ADDRCONFIG;

		struct addrinfo *addrlist;
		if (getaddrinfo("peertracker.bubcoin.org", BUBCOIN_PORT_AS_STRING, &hints, &addrlist) != 0)
		{
			bublog(LOG_ERROR, "failed to resolve any peers");
			bub_abort();
		};

		struct addrinfo *ai;
		for (ai=addrlist; ai!=NULL; ai=ai->ai_next)
		{
			if (ai->ai_family == AF_INET6)
			{
				struct sockaddr_in6 *addr6 = (struct sockaddr_in6*) ai->ai_addr;

				int index = numPeers++;
				peers = (struct in6_addr*) realloc(peers, sizeof(struct in6_addr) * numPeers);
				memcpy(&peers[index], &addr6->sin6_addr, sizeof(struct in6_addr));
			};
		};

		freeaddrinfo(addrlist);
	};

	if (numPeers == 0)
	{
		bublog(LOG_ERROR, "failed to find any peers");
		abort();
	};

	mtxPeers = mutex_new();
	net_save_peers();

	socket_t sock = socket(AF_INET6, SOCK_STREAM, 0);
	if (sock == SOCK_INVALID)
	{
		bublog(LOG_ERROR, "failed to create server socket");
		bub_abort();
	};

	int v6only = 0;
	if (setsockopt(sock, IPPROTO_IPV6, IPV6_V6ONLY, (const char*) &v6only, sizeof(v6only)) != 0)
	{
		bublog(LOG_ERROR, "failed to disable IPv6-only on the server socket");
		bub_abort();
	};

	struct sockaddr_in6 sa;
	memset(&sa, 0, sizeof(struct sockaddr_in6));
	sa.sin6_family = AF_INET6;
	sa.sin6_port = htons(BUBCOIN_PORT);

	if (bind(sock, (struct sockaddr*) &sa, sizeof(struct sockaddr_in6)) != 0)
	{
		bublog(LOG_ERROR, "server socket failed to bind to TCP port %d", BUBCOIN_PORT);
		bub_abort();
	};

	if (listen(sock, 5) != 0)
	{
		bublog(LOG_ERROR, "server socket failed to go into listening state");
		bub_abort();
	};

	serverSocket = sock;

	Thread *th = thread_start(net_server_thread, NULL);
	thread_detach(th);

	th = thread_start(net_query_thread, NULL);
	thread_detach(th);
};

static void net_block_announce_thread(void *data)
{
	BlockAnnounceThreadInfo *info = (BlockAnnounceThreadInfo*) data;

	struct sockaddr_in6 sa;
	memset(&sa, 0, sizeof(struct sockaddr_in6));
	sa.sin6_family = AF_INET6;
	net_get_random_peer(&sa.sin6_addr);
	sa.sin6_port = htons(BUBCOIN_PORT);

	socket_t sock = socket(AF_INET6, SOCK_STREAM, 0);
	if (sock == SOCK_INVALID)
	{
		bublog(LOG_ERROR, "net_block_announce_thread: failed to create socket");
		closesock(sock);
		free(info);
		return;
	};

	int v6only = 0;
	if (setsockopt(sock, IPPROTO_IPV6, IPV6_V6ONLY, (const char*) &v6only, sizeof(v6only)) != 0)
	{
		bublog(LOG_ERROR, "net_block_announce_thread: failed to disable IPv6-only on the socket");
		closesock(sock);
		free(info);
		return;
	};

	char addrAsStr[INET6_ADDRSTRLEN];
	inet_ntop(AF_INET6, &sa.sin6_addr, addrAsStr, INET6_ADDRSTRLEN);

	// TODO: timeout
	if (connect(sock, (struct sockaddr*) &sa, sizeof(struct sockaddr_in6)) != 0)
	{
		bublog(LOG_DEBUG, "net_block_announce_thread: failed to connect to %s", addrAsStr);
		closesock(sock);
		free(info);
		return;
	};

	MagicHeader head;
	memcpy(head.bub, "BUB", 3);
	head.msgType = 'A';
	head.resType = RES_BLOCK_DATA;

	if (send(sock, (const char*) &head, sizeof(MagicHeader), 0) != sizeof(MagicHeader))
	{
		bublog(LOG_ERROR, "net_block_announce_thread: failed to send header to %s", addrAsStr);
		closesock(sock);
		free(info);
		return;
	};

	if (send(sock, (const char*) &info->hash, sizeof(hash_t), 0) != sizeof(hash_t))
	{
		bublog(LOG_ERROR, "net_block_announce_thread: failed to send hash to %s", addrAsStr);
		closesock(sock);
		free(info);
		return;
	};

	if (send(sock, (const char*) info->data, info->size, 0) != info->size)
	{
		bublog(LOG_ERROR, "net_block_announce_thread: failed to send block to %s", addrAsStr);
	};

	closesock(sock);
	free(info);
};

static void net_trans_announce_thread(void *data)
{
	TransAnnounceThreadInfo *info = (TransAnnounceThreadInfo*) data;

	struct sockaddr_in6 sa;
	memset(&sa, 0, sizeof(struct sockaddr_in6));
	sa.sin6_family = AF_INET6;
	net_get_random_peer(&sa.sin6_addr);
	sa.sin6_port = htons(BUBCOIN_PORT);

	socket_t sock = socket(AF_INET6, SOCK_STREAM, 0);
	if (sock == SOCK_INVALID)
	{
		bublog(LOG_ERROR, "net_trans_announce_thread: failed to create socket");
		closesock(sock);
		free(info);
		return;
	};

	int v6only = 0;
	if (setsockopt(sock, IPPROTO_IPV6, IPV6_V6ONLY, (const char*) &v6only, sizeof(v6only)) != 0)
	{
		bublog(LOG_ERROR, "net_trans_announce_thread: failed to disable IPv6-only on the socket");
		closesock(sock);
		free(info);
		return;
	};

	char addrAsStr[INET6_ADDRSTRLEN];
	inet_ntop(AF_INET6, &sa.sin6_addr, addrAsStr, INET6_ADDRSTRLEN);

	// TODO: timeout
	if (connect(sock, (struct sockaddr*) &sa, sizeof(struct sockaddr_in6)) != 0)
	{
		bublog(LOG_DEBUG, "net_trans_announce_thread: failed to connect to %s", addrAsStr);
		closesock(sock);
		free(info);
		return;
	};

	MagicHeader head;
	memcpy(head.bub, "BUB", 3);
	head.msgType = 'A';
	head.resType = RES_TRANSACTION;

	if (send(sock, (const char*) &head, sizeof(MagicHeader), 0) != sizeof(MagicHeader))
	{
		bublog(LOG_ERROR, "net_trans_announce_thread: failed to send header");
		closesock(sock);
		free(info);
		return;
	};

	if (send(sock, (const char*) info->data, info->size, 0) != info->size)
	{
		bublog(LOG_ERROR, "net_trans_announce_thread: failed to send transaction");
	};

	closesock(sock);
	free(info);
};

void net_announce_block(hash_t *hash, const void *block, size_t size)
{
	int i;
	for (i=0; i<4; i++)
	{
		BlockAnnounceThreadInfo *info = (BlockAnnounceThreadInfo*) malloc(sizeof(BlockAnnounceThreadInfo) + size);
		info->size = size;
		info->hash = *hash;
		memcpy(info->data, block, size);

		thread_detach(thread_start(net_block_announce_thread, info));
	};
};

void net_announce_trans(const void *trans, size_t transSize)
{
	int i;
	for (i=0; i<4; i++)
	{
		TransAnnounceThreadInfo *info = (TransAnnounceThreadInfo*) malloc(sizeof(TransAnnounceThreadInfo) + transSize);
		info->size = transSize;
		memcpy(info->data, trans, transSize);

		thread_detach(thread_start(net_trans_announce_thread, info));
	};
};

void net_get_all_peers(struct in6_addr **out, int *outCount)
{
	mutex_lock(mtxPeers);
	
	struct in6_addr *peersCopy = (struct in6_addr*) malloc(sizeof(struct in6_addr) * numPeers);
	memcpy(peersCopy, peers, sizeof(struct in6_addr) * numPeers);
	*outCount = numPeers;
	
	mutex_unlock(mtxPeers);
	
	*out = peersCopy;
};