/*
	Bubcoin

	Copyright (c) 2020, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef BUBCOIN_SIG_H_
#define BUBCOIN_SIG_H_

#include "sha256.h"

/**
 * Represents a public key.
 */
typedef struct
{
	uint8_t data[64];
} pubkey_t;

/**
 * Represents a private key.
 */
typedef struct
{
	uint8_t data[32];
} privkey_t;

/**
 * Represents a signature.
 */
typedef struct
{
	uint8_t data[64];
} sig_t;

/**
 * Generate a keypair. Returns 0 on success, -1 on error.
 */
int sig_genkey(privkey_t *priv, pubkey_t *pub);

/**
 * Sign. Return 0 on success, -1 on error.
 */
int sig_sign(privkey_t *priv, sha256_t *ctx, sig_t *result);

/**
 * Returns nonzero if the signature is valid for the specified hash context.
 */
int sig_is_valid(pubkey_t *pub, sha256_t *ctx, sig_t *sig);

/**
 * Returns nonzero if the specified public key is valid.
 */
int sig_is_pubkey_valid(pubkey_t *pub);

#endif
