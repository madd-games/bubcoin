/*
	Bubcoin

	Copyright (c) 2020, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifdef __unix__

#include <pthread.h>
#include <stdlib.h>

#include "thread.h"
#include "log.h"

typedef struct
{
	thread_entry_t entry;
	void* param;
} ThreadEntryData;

struct Mutex_
{
	pthread_mutex_t pm;
};

struct Thread_
{
	pthread_t pt;
};

Mutex* mutex_new()
{
	Mutex *mtx = (Mutex*) malloc(sizeof(Mutex));
	pthread_mutex_init(&mtx->pm, NULL);
	return mtx;
};

void mutex_destroy(Mutex *mtx)
{
	pthread_mutex_destroy(&mtx->pm);
	free(mtx);
};

void mutex_lock(Mutex *mtx)
{
	pthread_mutex_lock(&mtx->pm);
};

void mutex_unlock(Mutex *mtx)
{
	pthread_mutex_unlock(&mtx->pm);
};

static void* posix_thread_entry(void *data_)
{
	ThreadEntryData *data = (ThreadEntryData*) data_;
	thread_entry_t entry = data->entry;
	void *param = data->param;
	free(data);
	
	entry(param);
	return NULL;
};

Thread* thread_start(thread_entry_t entry, void *param)
{
	ThreadEntryData *data = (ThreadEntryData*) malloc(sizeof(ThreadEntryData));
	data->entry = entry;
	data->param = param;
	
	Thread *th = (Thread*) malloc(sizeof(Thread));
	if (pthread_create(&th->pt, NULL, posix_thread_entry, data) != 0)
	{
		bublog(LOG_ERROR, "thread creation failed");
		bub_abort();
	};
	
	return th;
};

void thread_detach(Thread *th)
{
	pthread_detach(th->pt);
};

void thread_wait(Thread *th)
{
	pthread_join(th->pt, NULL);
};

void thread_exit()
{
	pthread_exit(NULL);
};

#endif /* __unix__ */