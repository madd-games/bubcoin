/*
	Bubcoin

	Copyright (c) 2020, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "config.h"

Config config;

void config_init()
{
	// set default config here
	config.minerThreads = 0;
	config.logDebug = 0;
	config.logInfo = 1;
	
	// load config from the file (if it exists)
	FILE *fp = fopen("bubcoin.conf", "r");
	if (fp != NULL)
	{
		char linebuf[1024];
		char *line;
		
		while ((line = fgets(linebuf, 1024, fp)) != NULL)
		{
			char *endline = strchr(line, '\n');
			if (endline != NULL) *endline = 0;
			
			if (*line == 0) continue;		// empty line
			if (*line == '#') continue;		// comment line
			
			char *cmd = strtok(line, " \t");
			char *param = strtok(NULL, "");
			
			if (cmd == NULL || param == NULL)
			{
				continue;
			};
			
			if (strcmp(cmd, "miner-threads") == 0)
			{
				sscanf(param, "%d", &config.minerThreads);
			}
			else if (strcmp(cmd, "log-debug") == 0)
			{
				config.logDebug = strcmp(param, "yes") == 0;
			}
			else if (strcmp(cmd, "log-info") == 0)
			{
				config.logInfo = strcmp(param, "yes") == 0;
			}
			else
			{
				fprintf(stderr, "bubcoind: warning: invalid configuration command `%s'", cmd);
			};
		};
		
		fclose(fp);
	};
	
	// write back the configuration
	fp = fopen("bubcoin.conf", "w");
	if (fp != NULL)
	{
		fprintf(fp, "# Bubcoin daemon configuration file\n\n");
		
		fprintf(fp, "# SYNTAX:\tminer-threads <count>\n");
		fprintf(fp, "# Decide how many CPU threads should be used for mining. A value of 0 disables mining.\n");
		fprintf(fp, "miner-threads %d\n\n", config.minerThreads);
		
		fprintf(fp, "# SYNTAX:\tlog-debug <yes|no>\n");
		fprintf(fp, "# Decide whether the daemon log should include debug messages.\n");
		fprintf(fp, "log-debug %s\n\n", config.logDebug ? "yes" : "no");
		
		fprintf(fp, "# SYNTAX:\tlog-info <yes|no>\n");
		fprintf(fp, "# Decide whether the daemon log should include informative messages.\n");
		fprintf(fp, "log-info %s\n\n", config.logInfo ? "yes" : "no");
		
		fclose(fp);
	};
};