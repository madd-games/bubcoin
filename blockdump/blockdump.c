/*
	Bubcoin

	Copyright (c) 2020, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#include "../bubcoind/block.h"

void tohex(const void *data, size_t size, char *put)
{
	uint8_t *scan = (uint8_t*) data;
	
	while (size--)
	{
		sprintf(put, "%02hhx", *scan++);
		put += 2;
	};
	
	*put = 0;
};

void* dump_header(void *data)
{
	BlockHeaderEntry *header = (BlockHeaderEntry*) data;
	char hexstr[512];
	
	printf("Block header\n");
	tohex(&header->prevBlockHash, sizeof(hash_t), hexstr);
	printf("  Previous block:   %s\n", hexstr);
	tohex(&header->minerID, sizeof(pubkey_t), hexstr);
	printf("  Mined by:         %s\n", hexstr);
	printf("  Timestamp:        %" PRIu64 "\n", header->timestamp);
	printf("\n");
	
	return &header[1];
};

void* dump_input(void *data)
{
	BlockTransactionEntryInput *input = (BlockTransactionEntryInput*) data;
	char hexstr[512];
	tohex(&input->inputBlockHash, sizeof(hash_t), hexstr);
	printf("    From %s[%" PRIu64 "]\n", hexstr, input->entryIndex);
	
	return &input[1];
};

void* dump_output(void *data)
{
	BlockTransactionEntryOutput *output = (BlockTransactionEntryOutput*) data;
	char hexstr[512];
	
	printf("  Output\n");
	tohex(&output->targetID, sizeof(pubkey_t), hexstr);
	printf("    Target:         %s\n", hexstr);
	printf("    Amount:         %d.%06d BUB\n", (int) (output->value / FULL_BUBCOIN), (int) (output->value % FULL_BUBCOIN));
	printf("\n");
	
	return &output[1];
};

void* dump_transaction(void *data)
{
	BlockTransactionEntryHeader *header = (BlockTransactionEntryHeader*) data;
	char hexstr[512];
	
	printf("Transaction\n");
	tohex(&header->spenderID, sizeof(pubkey_t), hexstr);
	printf("  Spender:          %s\n", hexstr);
	tohex(&header->signature, sizeof(sig_t), hexstr);
	printf("  Signature:        %s\n", hexstr);
	printf("  Timestamp:        %" PRIu64 "\n", header->timestamp);
	printf("\n");
	
	data = &header[1];
	printf("  Inputs\n");
	while (header->numInputs--)
	{
		data = dump_input(data);
	};
	printf("\n");
	
	while (header->numOutputs--)
	{
		data = dump_output(data);
	};
	
	return data;
};

void* dump_footer(void *data)
{
	BlockFooterEntry *footer = (BlockFooterEntry*) data;
	
	printf("Block footer\n");
	printf("  Difficulty:       %016" PRIx64 "\n", footer->difficulty);
	printf("  Nonce:            %016" PRIx64 "\n", footer->nonce);
	
	return NULL;
};

void* dump_next(void *data)
{
	switch (*((uint8_t*)data))
	{
	case BLK_ENT_HEADER:
		return dump_header(data);
	case BLK_ENT_TRANSACTION:
		return dump_transaction(data);
	case BLK_ENT_FOOTER:
		return dump_footer(data);
	default:
		printf("Invalid entry!\n");
		return NULL;
	};
};

int main(int argc, char *argv[])
{
	if (argc != 2)
	{
		fprintf(stderr, "USAGE:\t%s <block-file>\n", argv[0]);
		return 1;
	};
	
	FILE *fp = fopen(argv[1], "rb");
	if (fp == NULL)
	{
		perror("fopen");
		return 1;
	};
	
	fseek(fp, 0, SEEK_END);
	size_t size = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	
	void *data = malloc(size);
	if (fread(data, 1, size, fp) != size)
	{
		perror("fread");
		return 1;
	};
	
	fclose(fp);
	
	while (data != NULL)
	{
		data = dump_next(data);
	};
	
	return 0;
};